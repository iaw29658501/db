--() { :; }; exec psql template1 -f "$0"
/*
Nom: David Moraño Ureña
Codi: iaw47946292
*/
/*************************
-- Cursor scott:  --
*************************/


\c scott
drop function if exists get_emp_names();


/*************************************************************
get_even_emp_names_with_a
**************************************************************
    Task: 
		Retorna una cadena amb una llista d'empleats amb identificador parell i que el seu nom contingui la lletra a.
	In:
		N/A
	Out:
		-> (text): Llista de empleats amb identificadors parells que continguin la lletra 'a' al seu nom.. 
*************************************************************/



CREATE OR REPLACE FUNCTION get_emp_names()
RETURNS text
AS $$
DECLARE
	employers TEXT DEFAULT '';
	rec_emp   RECORD;
	cur_emps CURSOR  IS               
	select e.empno id, e.ename, d.dname dept, e.job, m.ename manager
	from emp e
	    join dept d on e.deptno = d.deptno
		full join emp m on e.mgr = m.empno
	where (e.empno % 2) = 0;	
BEGIN
	OPEN cur_emps;
	LOOP
		FETCH cur_emps INTO rec_emp;
		EXIT WHEN NOT FOUND;
			employers := employers || chr(10) || rec_emp.id || ' - ' || rpad(coalesce(rec_emp.ename, 'no name'), 10, ' ') || ' : ' || rpad(coalesce(rec_emp.job, '(no job)'), 10, ' ') || '(' || rpad(coalesce(rec_emp.dept, '(no dept)'), 10, ' ') || ') <- ' || coalesce(rec_emp.manager, '(no mgr)');
	END LOOP;
	CLOSE cur_emps;
	RETURN employers;
END;
$$ LANGUAGE plpgsql;

/*****************************
* JOC DE PROVES              *
*****************************/

select get_emp_names();

