-- Cauan Goes Mateos
-- iaw29658501

create or replace function tControlPedido() returns trigger as $control$
begin
	if TG_OP='UPDATE'
	then
		raise exception 'Cambiar pedidos concluidos no es permitido, comunicar el administrador';
	else
		if NEW.cant > ( select exist from producto p where (NEW.fabcod, NEW.prodcod) = (p.fabcod, p.prodcod))
			then
				raise exception 'Cuantidad pedida mayor de lo que tenemos';
			end if;
	end if;
end;
$control$ language plpgsql;

drop trigger tControlPedido on pedido;

create trigger tControlPedido 
before insert or update on pedido
for each row
execute procedure tControlPedido();


insert into pedido values ( nextval('pednum_seq'), current_date, 2117, 106, 'rei', '2a45c' , 211, 31500.00 );

update pedido set repcod = '106';
