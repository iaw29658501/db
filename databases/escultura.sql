-- Cauan Góes Mateos iaw29658501
drop table Museo,SalaMuseo,obra,autor cascade;
create table Museo(
	IdMuseo varchar(32) constraint IdMuseo_pk primary key,
	Nom varchar(64),
	Adreca varchar(128),
	IdCiudad varchar(64)
);

create table Sala(
	IdMuseo varchar(128) ,
	IdSala varchar(64)
);

create table obra(
	IdObra varchar(32) constraint IdObra_pk primary key,
	Titol varchar(64),
	Tipus varchar(32) constraint CK_Tipus check (Tipus in ('pintura','escultura')) ,
	IdAutor varchar(32) ,
	IdMuseo varchar(128),
	IdSala varchar(64)
);

create table autor(
	IdAutor varchar(32) constraint IdAutor_pk primary key,
	Nom varchar(128),
	Nacionalitat varchar(64)
);
create table Ciudad(
	IdCiudad varchar(32) constraint IDCiudad_pk primary key,
	Ciudad varchar(32),
	Pais varchar(32)
);
alter table sala add constraint sala_idmuseo_idsala_pk primary key (IdMuseo, IdSala);
--  alter table sala add contraint sala__IdMuseo_FK foreign key Museo(IdMuseo)
-- alter table obra add constraint obra_IdMuseo_IdSala_fk foreign key (IdMuseo,IdSala) references SalaMuseo(
