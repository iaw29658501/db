-- iaw29658501 Cauan Goes Mateos
--1. Obtener una lista de todos los     productos cuyo precio exceda de 3500 y de los cuales hay algún pedido con un importe superior a 35000.

Select prodcod, fabcod 
from producto
where precio > 3500
INTERSECT
select prodcod, fabcod
from pedido
where importe > 35000


--2. Obtener una lista de todos los productos cuyo precio más IVA exceda de 3.500 o bien haya algún pedido cuyo importe más IVA exceda de 30.000.
select prodcod, fabcod
from producto
where precio*1.21>3500
UNION
select prodcod, fabcod
from pedido
where importe*1.21>30000
 

-- 3. Obtener los códigos de los representantes que son directores de oficina y que no han tomado ningún pedido.

select diretor from oficina
except 
select repcod from repventa
