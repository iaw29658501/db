-- El codigo deve ser independente del contenido ( ponga coalesce tio )

---- Cauan Goes Mateos iaw29658501 07/02/2019

--1
insert into lloguer values
(1,2,currenttimestamp - interval '8 days', null, null);


-- 2 
delete from lloguer where datapres < current_timestamp - interval '7 days';

-- 3 Actualiza el lloguer anel Capdevila, el seu import amb el seu preu corresponent.

/*
select p.codpeli, p.titol from pelicula p, dvd d where p.codpeli=d.codpeli left join (select l.coddvd, count(*) from lloguer l group by coddvd) on

*/
-- 4  Mostra per cada peli el seu codi, titol, el numero de vegades que s'ha prestada i els guanys derivats del seuprestec (ultilizeu el camp preu, no import) El 3r i 4t camp es diran Prestecs i Guanys respectivament. Ordena la llista de pelis de mes vegades prestades a menys, i si ha coincidencia per ordre alfabetic
/*
select coddvd "dvds" , count(*) from lloguer group by coddvd
*/

-- 5 Mostrar per a cada genere, quina es la peli mes cara. Mostreu el nom del genere, el titol de la pelicula i el preu. Ordeneu per genere i titol.
-- falta poner el titol dela peli

select g.genere, max(preu) 
from pelicula p, genere g 
where g.codgen=p.codgen 
group by g.codgen 
order by 1;

/*
esta version tambien a sido casi pero pongo el group by en la subconsulta :( 
select distinct p.titol, top.gen , top.precio from pelicula p, (select g.genere "gen" , max(preu) "precio" 
from pelicula p, genere g 
where g.codgen=p.codgen 
group by g.codgen 
order by 1) as top where p.preu=top.precio and top.gen=(select ge.genere from genere ge where ge.codgen=p.codgen);
*/


-- 6 Mostra el generes dels quals mai s'ha prestat una peli. Ordena'ls alfabeticament
/*
(select l.coddvd "dvdpres", count(*) "lugadas" from lloguer l group by coddvd) "dvd_lug"
*/
-- 7
/* Mostra el titol de la pelicula (o peliculas) que te mes gent esperant per ella. Ordena alfabeticament

select p.titol 
from pelicula p, 
	(select codpeli "codipeli", count(*) "lista" 
	from llistaespera l 
	group by l.codpeli 
	order by lista 
	desc limit 1) as top 
where p.codpeli=top.codipeli
order by 1;


