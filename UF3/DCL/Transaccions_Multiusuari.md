
**Erik Perdigones Garcia** <ins>iaw49185512</ins>  

**Cauan Goes Mateos** <ins>iaw29658501</ins>  

**Pol Núñez Rubio** <ins>iaw47671052</ins>  


--------------------------------------------
   Funcions de transaccions
--------------------------------------------

1.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  

```
INSERT INTO punts (id, valor) VALUES (10,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 10;
ROLLBACK;
SELECT valor FROM punts WHERE id = 10;
```  
**Se ha hecho un update donde cambiaba el valor, pero al hacer un rollback se vuelve al inicio de la transaccion, donde recupera su valor original.**

2.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  
```
INSERT INTO punts (id, valor) VALUES (20,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 20;
COMMIT;
SELECT valor FROM punts WHERE id = 20;
```
**Se guarda el nuevo valor introducido en la tabla, no se recupera el antiguo.**

3.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
INSERT INTO punts (id, valor) VALUES (30,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (31,7);
ROLLBACK;
SELECT valor FROM punts WHERE id = 30;
```  

**Mantiene el valor de 5 en la id 30, el rollback es desde el principio de la transaccion. Tampoco se crea la nueva fila de id x31.**

4.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (40,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*) FROM punts;
```
**Mantiene el valor nuevo de la id 40, pero el rollback es hasta el savepoint a, asi que la nueva fila con id 41 no aparecera.**  

5.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
INSERT INTO punts (id, valor) VALUES (50,5);
BEGIN;
SELECT id, valor WHERE punts;
UPDATE punts SET valor = 4 WHERE id = 50;
COMMIT;
SELECT valor FROM punts WHERE id = 50;
```
**El comando de la tercera linia da error, asi que hasta que no se finaliza la transicion no hay ningun tipo de update. Por eso se mantiene el resultado del primer valor introducido.**  

6.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (60,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor) VALUES (61,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor) FROM punts;
```  

**Se van realizando cambios hasta llegar al savepoint b, la siguiente orden no tendra influencia porque hay un rollback hasta el savepoint b, la suma mostrara la suma de 4 + 8, que respectivamente son las ids de 60 y 61.**  

7.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
DELETE FROM punts; -- Connexió 0
INSERT INTO punts (id, valor) VALUES (70,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

SELECT COUNT(*) FROM punts; -- Connexió 2
```  

**No podra ver la borrada de la tabla punts, hasta que no se finalice la transición. Asi que el usuario que haga select, le mostrara el resultado 1.**  

8.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
INSERT INTO punts (id, valor) VALUES (80,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (81,9); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 4 WHERE id = 80; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 81; -- Connexió 2

UPDATE punts SET valor = 10 WHERE id = 81; -- Connexió 1

UPDATE punts SET valor = 6 WHERE id = 80; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 80; -- Connexió 0
```  

**La conexion 2 no podra realizar su transaccion puesto que la connexión 1 empezo antes la transición y no ha hecho un commit, eso conlleva a que la gran mayoria de cambios el tendra la preferencia.**  

9.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
INSERT INTO punts (id, valor) VALUES (90,5); -- Connexió 0

BEGIN; -- Connexió 1
-- el tendra una fila en su 'foto'
DELETE FROM punts; -- Connexió 1    # deleta o q tem na sua 'foto' o seja só o id 90
-- el borro su unica fila
BEGIN; -- Connexió 2
INSERT INTO punts (id, valor) VALUES (90,9); -- Connexió 2
-- el dos vera dos filas
COMMIT; -- Connexió 2
-- el dos vera dos filas
-- el uno vera una fila nueva insertada por la conexion 2
COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 91; -- Connexió 0
```  

**Aunque en la conexxión uno se haga un borrado de la tabla, la connexión 2 esta creando una fila nueva que no exisita antes del borrado de la tabla. Por eso cuando inserta una nueva fila (id=91) no causa ningun tipo de error. En cambio la fila id=90 es borrada por el comando de la connexión 1.**

10.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
INSERT INTO punts (id, valor) VALUES (100,5); -- Connexió 0

grant 

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 100; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 100; -- Connexió 0
```
**EL usuario de la connexión 1 ha empezado la transacción antes, cambiando el valor del punto id=100. Como el usuario de la connexión 2 ha empezado la transacción más tarde y queria editar el valor que habia cambiado la connexión 1, se queda bloqueado hasta que el usuario de la connexión 1 finalice su transacción. El valor final sera 7.**

11.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
INSERT INTO punts (id, valor) VALUES (110,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (111,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 110; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 110; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 111; -- Connexió 2
SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 110; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 111; -- Connexió 0
```
**La connexión 1 comienza la transacción cambiando el valor de la id=110 y sin finalizar la transacción, la connexión 2 empieza una transacción cambiado también el valor de la id=110, lo que lleva a que se bloquee. Continua ejecutando comandos cambiando el valor de la id=111, haciendo un savepoint y volviendo a cambiar el valor del id=110. Pero luego la connexión 2 hace un rollback al savepoint a, cancelando el ultimo cambio de valor del id=110. Cuando la connexión 1 ejecuta el commit, se efectuan todos los cambios como en el ejercicio anterior y el resultado final muestra los valores que ha hecho la connexión 2 antes del savepoint. los valores finales son 7 y 7, respectivamente.**

12.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
INSERT INTO punts (id, valor) VALUES (120,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (121,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 121; -- Connexió 1
SAVEPOINT a;
UPDATE punts SET valor = 9 WHERE id = 120; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 120; -- Connexió 2

ROLLBACK TO a; -- Connexió 1

SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 120; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 121; -- Connexió 0
```
**La connexión 1 ha hecho un cambio de valor en el id=121 y a continuación hace un savepoint antes de cambiar el valor de id=120. Entonces la connexión 2 empieza una transacción haciendo un update del valor en la id=120 (cosa que llevara a que se bloquee). La novedad es que la connexión 1 hace rollback al savepoint, cancelando el update del valor de la id=120, que conlleva a que el update de la connexión 2 se ejecute, ya que el valor de la id=120 recupero su valor original antes de cualquier transacción. el valor de id=120 es 7 y de id=121 es 6.**

