-- iaw29658501
-- Cauan Goes Mateos

drop function instr(origen varchar, patro varchar);
create or replace function instr(origen varchar, patro varchar)
returns varchar
AS $$
        BEGIN
                RETURN strpos(origen, patro);
        END;
$$ LANGUAGE plpgsql;

drop function strcat(cadena1 varchar, cadena2 varchar);
create or replace function strcat(cadena1 varchar, cadena2 varchar)
RETURNS varchar
AS $$
BEGIN
RETURN cadena1 || cadena2;
END;
$$ Language plpgsql;


