-- iaw29658501 Cauan Goes
--\c biblioteca

create or replace function prestecDocument(p_idusuari prestec.idusuari%TYPE, p_iddocument exemplar.iddocument%TYPE ) returns boolean as $$
begin
-- Si el cliente ya tiene 4 o mas en este momento alugados o
-- si esta bloqueado no se podera prestar
	if ( ((select count(*) 
		from prestec 
		where idusuari=p_idusuari 
		and datadev is null) >  3 )
		 or
		( select idusuari
		from usuari
		where bloquejat IS not null and
		idusuari=p_idusuari) > 0)

		THEN
		RAISE NOTICE 'maximo de 4 por persona, tu ya tienes el maximo';
		return false;
	end if;

-- si quiere alugar un libro y ya tiene dos alugados no podre
	if ((select 1 
		from llibre 
		where iddocument=p_iddocument) > 0
		and
		(select count(*) 
		from prestec 
		where idusuari=p_idusuari 
		and idexemplar in 
			( select idexemplar 
			from llibre) ) >=  2 )

		THEN
		RAISE NOTICE 'MAXIMO DE DOS LIBROS POR PERSONA AL MISMO TIEMPO';
		return false;
	end if;

-- si quiere alugar un cd y ya tiene un alugado no podra
	if ((select 1 
		from musica 
		where iddocument=p_iddocument) > 0
		and
		(select count(*)
		from prestec
		where idusuari=p_idusuari 
		and idexemplar in 
			( select idexemplar from musica))
		>=  1 )
		THEN
		RAISE NOTICE 'MAXIMO DE UN CD POR PERSONA AL MISMO TIEMPO';
		return false;
	end if;


-- si quiere alugar un dvd y ya tiene un alugado no podra
	if ((select 1 
		from pelicula 
		where iddocument=p_iddocument) > 0
		and
		(select count(*)
		from prestec
		where idusuari=p_idusuari 
		and idexemplar in 
			( select idexemplar from pelicula))
		>=  1 )
		THEN
		RAISE NOTICE 'MAXIMO DE UN DVD POR PERSONA AL MISMO TIEMPO';
		return false;
	end if;
	return true;
end;
$$ language plpgsql;

insert into prestec values (1, current_timestamp, null, 2);

select prestecDocument(1,3);
select prestecDocument(2,5);


create or replace function renovarDocument(p_idexemplar exemplar.idexemplar%type) 
returns boolean as $$
declare v_idusuari prestec.idusuari%type;
begin
	select idusuari 
	INTO STRICT v_idusuari
	from prestec 
	where datadev is null 
	and idexemplar=p_idexemplar;	

IF ( select bloquejat from usuari where idusuari=v_idusuari ) then
		RAISE NOTICE 'Usuari % bloqueado', v_idusuari;
		return false;
end if;

	update prestec
        set datadev=current_timestamp
	where datadev is null
	and idexemplar=p_idexemplar;

	insert into prestec
	values ( p_idexemplar , current_timestamp, null, v_idusuari);	
	RAISE NOTICE 'Exemplar % renovado', p_idexemplar;
	return true;

end;
$$ language plpgsql;

insert into prestec values ( 2, current_timestamp, null, 5);
select renovarDocument(4);

create or replace function retornarDocument(p_iddocument document.iddocument%type, p_idusuari prestec.idusuari%type)
returns boolean as $$
declare v_idexemplar exemplar.idexemplar%type;
BEGIN
	select idexemplar 
	into strict v_idexemplar 
	from exemplar 
	where iddocument=p_iddocument;

	update prestec 
	set datadev=current_timestamp
	where idusuari=p_idusuari
	and idexemplar=v_idexemplar
	and datadev is null;
	raise notice 'Registrado retorno del documento % por %',p_iddocument, p_idusuari;

	return true;
END;
$$ language plpgsql;

insert into prestec values ( 6, current_timestamp, null, 5);
select retornarDocument(3,5);


create or replace function reservarDocument( p_iddocument document.iddocument%type, p_idusuari usuari.idusuari%type)
returns boolean as $$
declare v_idexemplar exemplar.idexemplar%type;
BEGIN
	select idexemplar
	into strict v_idexemplar 
	from exemplar
	where iddocument=p_iddocument 
	and datadev is null
	order by datapres 
	limit 1;
	insert into reserva values (v_idexemplar, p_idusuari, current_timestamp, null );
	raise notice 'Reserva del exemplar % adicionada para %',p_idexemplar, p_idusuari;
	return true;
end;
$$ language plpgsql;