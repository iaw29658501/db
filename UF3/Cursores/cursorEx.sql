--() { :; }; exec psql proves -f "$0"
CREATE OR REPLACE FUNCTION get_film_titles()
RETURNS text
AS $$
DECLARE
 titles TEXT DEFAULT '';
    -- declarem una variable fila anomenada rec_film, la qual reculirà cada fila
    -- que es llegeix (fetch) del cursor
    -- Oracle permet %rowtype per taules i cursors, postgreSQL per taules només
 rec_film   RECORD;
    -- Els cursors poden admetre paràmetres.
    -- Aquest és un exemple de cursor bàsic, sense paràmetres
 cur_films CURSOR  IS               -- postgreSQL usa habitualment "FOR"
     SELECT title, release_year     -- pero permet "IS" com a gest cap  a Oracle
     FROM film;
BEGIN
	   -- Open the cursor
   OPEN cur_films;
   LOOP
	      -- fetch row into the film
      FETCH cur_films INTO rec_film;
      -- exit when no more row to fetch
      -- La sintaxi de postgreSQL difereix una
      -- mica d'Oracle, que sería així:
      -- EXIT WHEN cur_films%NOTFOUND;
      EXIT WHEN NOT FOUND;
      -- build the output
      IF rec_film.release_year = '2019' THEN
	         titles := titles || E'\n' || rpad(rec_film.title,30,' ')  || ':' || rec_film.release_year;
		      END IF;
		   END LOOP;
		   -- Close the cursor
   CLOSE cur_films;

   RETURN titles;
END;
$$ LANGUAGE plpgsql;


select get_film_titles();
