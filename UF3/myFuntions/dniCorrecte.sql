-- Cauan Goes
-- iaw29658501

drop function if exists dniCorrecte( p_dni varchar);
	
create or replace function dniCorrecte( p_dni varchar ) returns varchar
as $$
DECLARE v_numeros BIGINT;
BEGIN
	v_numeros := substring( p_dni from '[0-9]{8}' )::bigint;
	return v_numeros||substring('TRWAGMYFPDXBNJZSQVHLCKET',(MOD(v_numeros,23)+1)::int,1);
END;
$$ Language plpgsql;

drop function if exists dniCorrecte( p_dni bigint);

create or replace function dniCorrecte( p_dni bigint ) returns varchar
as $$
begin
	return dniCorrecte(p_dni::varchar);
end;
$$ Language plpgsql;

select dniCorrecte( '29658501' );
select dniCorrecte( 29658501 );

