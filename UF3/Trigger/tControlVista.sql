-- iaw29658501
-- Cauan Goes Mateos
-- ex accounting new york y puede ser que el departamento no exista ex informatica BCN 
create or replace function tControl() 
returns trigger 
as $$
begin
	if TG_OP = 'INSERT' 
	then
		INSERT INTO emp values ( default, NEW.nomemp, NEW.ofici,  current_timestamp, TG_OP );
	end if;
	return NEW;
end;
$$ language plpgsql;
drop trigger tControl on emp;


DROP trigger tControl on empleat;
CREATE trigger tControl
INSTEAD OF update or insert or delete ON empleat
FOR EACH ROW
EXECUTE PROCEDURE tControl();

update emp 
set deptno=20 
where empno > 7900;
