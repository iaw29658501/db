-- iaw29658501 Cauan Goes Mateos 1HIAW
/*
1.
Mostra el titol de la pel.lícula, el nom del seu director així com el
seu gènere (no el codi).
*/
select titol, g.genere, d.nom "diretor" from pelicula p, genere g, director d where g.codgen=p.codgen;

/*
2.
Es vol saber quins dvds hi ha per cada peli. Es demana el codi de
cada pel.lícula, el títol, i el codi del dvd.
*/
select p.codpeli, p.titol, d.coddvd from dvd d, pelicula p where p.codpeli=d.codpeli;
/*
3.
Es vol saber les pel.lícules que actualment estan en préstec. Mostrar
el nom del soci, el nom de la peli, la data de préstec i la devolució.
*/
select s.nom, p.titol, l.datapres "préstec" from lloguer l, so
ci s, pelicula p, dvd d where datadev is null and s.codsoci=l.codsoci and
d.coddvd=l.coddvd and p.codpeli=d.codpeli;
/*
4.
Es vol saber les pel.lícules que en el passat van estar llogades.
Mostrar el nom del soci, el nom de la peli, la data de préstec i la
devolució.
*/
select s.nom soci,p.titol, l.datapres from lloguer l, soci s,dvd d, pelicula p where datadev is not null and l.codsoci=s.codsoci and d.coddvd=l.coddvd and p.codpeli=d.codpeli;

/*
5.
Es vol mostrar la llista d’espera de socis i pelis, ordenada per codi
de peli i per data ascendentment. Mostreu nom de la peli, nom del
soci i data de reserva.
*/
select l.codpeli, p.titol, s.nom, l.data_res from llistaespera l, pelicula p, soci s where s.codsoci=l.codsoci and p.codpeli=l.codpeli order by 1, 4 asc;
/*
6.
Mostra el repartiments de totes les pelis. Mostra el títol de la peli i
el nom dels actors.
*/
select p.titol, a.nom from pelicula p, repartiment r, actor a where p.codpeli=r.codpeli and a.codactor=r.codactor;
/*
1.­ Obtener los nombres de los socios que tienen actualmente prestada una película que
ya tuvieron prestada con anterioridad.
select * from lloguer l where datadev is null and (codsoci, coddvd) in */
(select codsoci,coddvd from lloguer ll where ll.codsoci != l.codsoci and ll.coddvd != l.coddvd);
/*
2.­ Obtener el título de la película o películas que han sido prestadas más veces.
*/
select coddvd, count(*) from lloguer group by coddvd order by coddvd ;
/*
3.­ Obtener el título de las películas que han sido prestadas a todos los socios del
videoclub.
*/
/*
4.­ Obtener el nombre y la dirección del socio o socios más peliculeros, es decir, los que
han tomado el mayor número de películas distintas.
*/
select l.codsoci, count(distinct (l.codsoci,l.coddvd)) "count" from lloguer l group by codsoci order by 2 desc ;
/*
5.­ Obtener los títulos de las películas que nunca han sido prestadas.
*/
select p.titol from pelicula p, dvd d where p.codpeli=d.codpeli and d.coddvd not in (select coddvd from lloguer);
/*
6.­ Obtener el título de la película o películas cuya lista de espera es la más larga.
/*
--???
/*

7.­ Obtener los nombres de los socios que han tomado prestada la película Blancanieves
alguna vez o que están esperando para tomarla prestada.
*/
select titol from pelicula p where p.codpeli in (select distinct codpeli from llistaespera) or p.codpeli in (select codpeli from lloguer);
/*
8.­ Obtener los nombres de los socios que han tomado prestada la película Blancanieves
alguna vez y que además están en su lista de espera.

9.­ Obtener una lista de los géneros cinematográficos de los que se han realizado más de
3  prestamos.
10.­ Nombre y teléfono de las personas que llevan más tiempo en la lista de espera para
la película “Lo que el viento se llevó”
11.­ Obtener los datos del socio que ha tomado prestadas más películas del actor Alex
Bonnin.
*/

/* No estava en este dia que tu no venias y perdi la explicacion :P */
