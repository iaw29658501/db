--() { :; }; exec psql template1 -f "$0"
/*
Nom: David Moraño Ureña
Codi: iaw47946292
*/
/*************************
-- Cursor scott (versió while):  --
*************************/


\c scott
drop function if exists get_emp_names();


/*************************************************************
get_emp_names
**************************************************************
    Task: 
		Retorna una cadena amb una llista d'empleats.
	In:
		N/A
	Out:
		-> (text): Llista de empleats. 
*************************************************************/



CREATE OR REPLACE FUNCTION get_emp_names()
RETURNS text
AS $$
DECLARE
	rec_emp   RECORD;
	cur_emps CURSOR  IS               
	select e.empno id, e.ename, d.dname dept, e.job, m.ename manager
	from emp e
	    join dept d on e.deptno = d.deptno
		full join emp m on e.mgr = m.empno
	where (e.empno % 2) = 0;	
BEGIN
	OPEN cur_emps;
	FETCH cur_emps INTO rec_emp;
	WHILE FOUND
	LOOP
		raise notice E'ID: %, NAME: %, JOB: %, DEPT: %, MGR: %', 
				rec_emp.id, 
				rpad(coalesce(rec_emp.ename, '---'), 8, ' '), 
				rpad(coalesce(rec_emp.job, '---'), 10, ' '),  
				rpad(coalesce(rec_emp.dept, '---'), 10, ' '),
				coalesce(rec_emp.manager, '---');
		FETCH cur_emps INTO rec_emp;
	END LOOP;
	CLOSE cur_emps;
	RETURN ''::text;
END;
$$ LANGUAGE plpgsql;

/*****************************
* JOC DE PROVES              *
*****************************/

select get_emp_names();

