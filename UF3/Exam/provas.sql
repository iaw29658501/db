-- grupo completo true
select grupoCompleto('M01', 2::smallint);

-- false
select grupoCompleto('M01',1::smallint);
-- exception
select grupoCompleto('M12', 3::smallint);

/*
-- false
select asignaturasPendientes('M01', '12345678Z' );

-- true
select asignaturasPendientes('M08', '123455678Z');
*/

-- exception

select estaAprobada('M12', '22222222Z');

--true
select estaAprobada( 'M01', '12345678Z' );

-- false

select estaAprobada('M02', '87654321X');

