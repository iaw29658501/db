/*
Nom: David Moraño Ureña
Codi: iaw47946292
*/
/*************************
-- provaCursors --
*************************/

\c scott

declare
	cursor c_emp is
		select ename, sal
		from emp
		where sal > 2000;
	v_ename emp.ename%type;
	v_sal emp.sal%type;
begin
	open c_emp;
	loop
		fetch c_emp into v_ename, v_sal

end;


