--() { :; }; exec psql proves -f "$0"
-- iaw29658501 Cauan Goes Mateos

\c scott
CREATE OR REPLACE FUNCTION get_emp()
RETURNS text
AS $$
DECLARE
 titles TEXT DEFAULT '';
    -- declarem una variable fila anomenada rec_emp, la qual reculirà cada fila
    -- que es llegeix (fetch) del cursor
    -- Oracle permet %rowtype per taules i cursors, postgreSQL per taules només
 rec_emp   RECORD;
    -- Els cursors poden admetre paràmetres.
    -- Aquest és un exemple de cursor bàsic, sense paràmetres
 cur_emp CURSOR  IS               -- postgreSQL usa habitualment "FOR"
     SELECT ename, job     -- pero permet "IS" com a gest cap  a Oracle
     FROM emp;
BEGIN
	   -- Open the cursor
   OPEN cur_emp;
   FETCH cur_emp into rec_emp;
   while found 
   LOOP
      -- exit when no more row to fetch
      EXIT WHEN NOT FOUND;
      -- build the output
      IF true THEN
	         titles := titles || E'\n' || rpad(rec_emp.ename,30,' ')  || ':' || rec_emp.job;
      END IF;
      FETCH cur_emp INTO rec_emp;
	END LOOP;

		-- Close the cursor
   CLOSE cur_emp;

   RETURN titles;
END;
$$ LANGUAGE plpgsql;


select get_emp();
