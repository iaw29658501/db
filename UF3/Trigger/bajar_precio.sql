--() { :; }; exec psql template1 -f "$0"
-- iaw29658501
-- Cauan Goes Mateos

-- Creando funcion que solo deje bajar los precios
create or replace function bajar_precio() returns trigger as $bajar_precio$
begin
	if NEW.precio > OLD.precio
		then
			raise exception 'Tienes de bajar el precio';
	end if;
	return NEW;

end;
$bajar_precio$ language plpgsql;

drop trigger bajar_precio on producto;
create trigger bajar_precio 
before update on producto
for each row
execute procedure bajar_precio();

-- prueba

update producto set precio=11111 where (fabcod, prodcod)=('rei','2a45c');
