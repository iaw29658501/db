--() { :; }; exec psql template1 -f "$0"
/*
Nom: David Moraño Ureña
Codi: iaw47946292
*/
/*************************
-- Script examen UF3: BD matricula:  --
*************************/

\c matricula
-------------------------------------------------------

drop function if exists grupoCompleto(varchar, int, varchar);

/*************************************************************
Funció grupoCompleto
**************************************************************
	Task: 
		Comprova si un grup donat està complet
	In: 
		-> p_codasig 
		-> p_codgrupo 
		-> p_cursoacad
	Out:
		-> True si el grup es troba complet, altrament, false
*************************************************************/


create or replace function grupoCompleto(p_codasig varchar, p_codgrupo int, p_cursoacad varchar)
returns boolean 
as $$
declare
	numAlumnosMatriculados int;
	numAlumnosMax int;
begin
	-- Contar alumnos que están matriculados en el grupo dado en el año actual
	select count(*)
	into strict numAlumnosMatriculados
	from expediente
	where idmodulo = p_codasig
		and codgrupo = p_codgrupo
		and cursoacad = p_cursoacad;
	-- Obtener número de alumnos límite
	select capacidad
	into strict numAlumnosMax
	from expediente e
		join grupo g on e.codgrupo = g.codgrupo
	where 
		cursoacad = p_cursoacad
		and e.codgrupo = p_codgrupo
	group by e.codgrupo, capacidad;
	-- Devolvemos la comparación de los valores obtenidos
	return numAlumnosMatriculados >= numAlumnosMax;
end
$$ language plpgsql;



-------------------------------------------------------

drop function if exists estaAprobada();



/*************************************************************
Funció estaAprobada 
**************************************************************
	Task: 
		Comprueba si para un alumno dadouna asignatura está aprobada.
	In: 
		-> p_codasig
		-> p_dni
	Out:
		-> True si la asignatura está aprobada para ese alumno (existe una cualificación no inferior a 5 en el expediente), en caso contrario false
*************************************************************/

create or replace function estaAprobada(p_codasig varchar, p_dni varchar)
returns boolean
as $$
declare
	aprobado boolean;
begin
	-- Ejecutamos una consulta y almacenamos el resultado en una variable que se devuelve posteriormente:
	--	- Obtenemos la nota del alumno dado para la asignatura dada (si no existe, la consideraremos 0)
	--  - Si la nota es igual o superior a 5, se considera aprobado.
	-- EN caso de que no exista ninguna fila de resultado, significa que el alumno no está matriculado, por tanto, se asume como false
	select coalesce(nota, 0) >= 5.00
	into strict aprobado
	from expediente
	where dni = p_dni
	and idmodulo = p_codasig;
	return aprobado;
exception
	when no_data_found then
	-- Matrícula no existe
--		raise notice E'El alumno con DNI=''%'' no está matriculado en ''%''.', p_dni, p_codasig; 
		return false;
end
$$ language plpgsql;

------------------------------------

drop function if exists asignaturasPendientes(varchar, varchar);



/*************************************************************
Funció asignaturasPendientes 
**************************************************************
	Task: 
		Comprueba si para un alumno dadoquedan asignaturas pendientes por aprobar de cara a realizar la matrícula a una asignatura dada. Para establecer si una asignatura está pendiente se consideran los siguientes casos:
		- Alguna asignatura requisito no tiene una calificación de aprobado para el alumno, ya sea por calificación insuficiente, por no evaluación, o bien por no matriculación previa.
	In: 
		-> p_codasig
		-> p_dni
	Out:
		-> True si quedan asignaturas pendientes por aprobar de cara a matricularse en la asignatura pasada como argumento, en caso contrario, false
*************************************************************/

create or replace function asignaturasPendientes(p_codasig varchar, p_dni varchar)
returns boolean
as $$
declare
	rec_asignPre record;
	cur_asignPre cursor is
		select idprerre from prerrequisito
		where idmodulo = p_codasig;
begin
	-- Utilizamos un cursor que contendrá los prerrequisitos de la asignatura parámetro. Para cada una, utilizaremos la función estaAprobada. Si alguna no está aprobada, devolveremos true.
	for rec_asignPre in cur_asignPre
	loop
		if not estaAprobada(rec_asignPre.idprerre, p_dni) then
			return true;
		end if;
	end loop;
	return false;
end
$$ language plpgsql;



-------------------------------------------

drop function if exists matriculaAsignatura(varchar, varchar, int, varchar);



/*************************************************************
Funció matriculaAsignatura 
**************************************************************
	Task: 
		Da de alta en el expediente la matrícula de un alumno en una asignatura, en un grupo de un curso académico cumplidos los siguientes requisitos:
		- El grupo en el que se quiere matricular no está completo
		- El expediente del alumno satisface los requisitos del módulo en cuestión.
	In: 
		-> p_dni
		-> p_codasig
		-> p_codgrupo
		-> p_cursoacad
	Out:
		-> Mensaje informando del resultado del proceso de matriculación.
*************************************************************/

create or replace function matriculaAsignatura(p_dni varchar, p_codasig varchar, p_codgrupo int, p_cursoacad varchar)
returns varchar
as $$
begin
	-- Se comprueba que el grupo en el que quiere matricularse no esté completo
	if grupoCompleto(p_codasig, p_codgrupo, p_cursoacad) then
		return 'NO SE PUDO TRAMITAR LA MATRÍCULA: El grupo ' || p_codgrupo || ' para la asignatura ' || p_codasig || ' está completo.';
	end if;
	-- Se comprueba que no existan prerrequisitos pendientes para que el alumno pueda matricularse	
	if asignaturasPendientes(p_codasig, p_dni) then
		return 'NO SE PUDO TRAMITAR LA MATRÍCULA: El alumno con DNI=' || p_dni || ' no tiene aprobadas todas las asignaturas requeridas para ' || p_codasig || '.';
	end if;
	-- Tramitamos matrícula, ya que los requisitos se cumplen.
	
	insert into expediente values
	(p_dni, p_cursoacad, p_codasig, p_codgrupo, null, null);
	return 'Matrícula tramitada con éxito.';
end
$$ language plpgsql;

-------------------------------------------

drop trigger if exists tMatricula on expediente;
drop function if exists actualizarDatosMatricula();
drop sequence if exists nummatri_seq;


/*************************************************************
Función ActualizarDatosMatricula 
**************************************************************
	Task: 
		Realiza operaciones colaterales en caso de realizar una matrícula.
		Al insertar en expediente, colateralmente se comprueba si existe un registro de matrícula para los datos de expediente añadidos.
			- Si no existe lo crea.
			- Si existe, actualiza precio.
	Out:
		-> (trigger) 
*************************************************************/


create sequence nummatri_seq;
select setval('nummatri_seq', (select max(nummatri) from matricula)::int);

create or replace function actualizarDatosMatricula()
returns trigger
as $$
declare
	v_nummatri int;
begin
	select nummatri
	into v_nummatri
	from matricula
	where dni = new.dni
		and cursoacad = new.cursoacad;

	if not found then
		-- No existía matrícula previa. Añadir
		v_nummatri := nextval('nummatri_seq');
		insert into matricula
		values
		(v_nummatri, new.dni, new.cursoacad, null);
	end if;
	-- Actualizar importe matrícula con el de la materia
	update matricula
	set importe = coalesce(importe, 0) + (
		select precio
		from modulo
		where idmodulo = new.idmodulo
	)
	where nummatri = v_nummatri;
	return new;
end
$$ language plpgsql;


/*******************************************
Trigger tMatricula
*******************************************/
create trigger tMatricula
after insert on expediente
for each row
execute procedure actualizarDatosMatricula();















