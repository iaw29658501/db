-- iaw29658501
-- Cauan Goes Mateos
-- 1. Creeu una funció que es digui darrerDiaMes, la qual mostrarà per pantalla el darrer dia del mes del mes actual. Sense arguments. Només podeu utilitzar funcions estandar (to_char)
\c template1
drop database cauan;
create database cauan;
\c cauan;

drop function darrerDiaMes();
CREATE OR REPLACE FUNCTION darrerDiaMes()
RETURNS VARCHAR
    AS $$
        declare year integer;
        declare month integer;
	BEGIN
            year := to_char(current_date,'YYYY');
            month := to_char(current_date,'MM');
        RETURN TO_CHAR(TO_DATE(year || '-' || month || '-01', 'YYYY-MM-DD') + INTERVAL '1 month' - INTERVAL '1 days','YYYY-MM-DD');
	END; $$
LANGUAGE plpgsql;

SELECT darrerDiaMes();
-- 2. Creeu la funció darrerDiaMes2 la qual, donat el més en numèric, mostrarà el darrer dia del mes.
DROP FUNCTION darrerDiaMes2( mon int );						
CREATE OR REPLACE FUNCTION darrerDiaMes2( mon int )
RETURNS VARCHAR
	AS $$ 
		declare year integer;
	BEGIN
		year := to_char(current_date,'YYYY');
	RETURN TO_CHAR(  TO_DATE(year || '-' || mon || '-01', 'yyyy-mm-dd') + INTERVAL '1 month' - INTERVAL '1 day', 'yyyy-mm-dd');
	END; $$
LANGUAGE plpgsql;
SELECT darrerDiaMes2(4);						
-- 3. Creeu la funció darrerDiaMes3, la qual donat un mes (text) passat com argument, mostrarà el darrer dia del mes.
DROP FUNCTION darrerDiaMes3( mon varchar );
CREATE OR REPLACE FUNCTION darrerDiaMes3( mon varchar )
RETURNS VARCHAR
	AS $$ 
		declare month integer;
	BEGIN
		-- case JANERO OR Jan THEN 1 ...
		month := mon::integer; 
	RETURN darrerDiaMes2(month);
	END; $$
LANGUAGE plpgsql;

SELECT darrerDiaMes3('4');
-- 4. Creeu la funció darrerDiaMesv2, que farà el mateix que darrerDiaMes i no tindrà arguments, utilitzant la funció darrerDiaMes2.     
DROP FUNCTION darrerDiaMesv2();
CREATE OR REPLACE FUNCTION darrerDiaMesv2()
RETURNS VARCHAR
    AS $$
        declare month integer;
	BEGIN
            month := to_char(current_date,'MM')::integer;
        RETURN darrerDiaMes2(month);
	END; $$
LANGUAGE plpgsql;
SELECT darrerDiaMesv2();
