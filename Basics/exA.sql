-- Cauan Goes iaw29658501
--------1 Obtener los códigos de los representantes que han tomado algún pedido (evitando su repetición).---------
select distinct repcode from repventa;
-------- 2 Obtener los datos de los pedidos realizados por el cliente cuyo código es el 2111.----------- 
select * from cliente where cliecod=2111;
------- 3 Obtener los datos de los productos cuyas existencias estén entre 25 y 40 unidades.----------
select * from producto where exist between 25 and 40;
-------- 4 Obtener los datos de los pedidos realizados por el cliente cuyo código es el 2111 y que han sido tomados por el representante cuyo código es el 103.---------
select * from pedido where cliecod=2111 and repcod=103;
-------- 5 Obtener los datos de los pedidos realizados por el cliente cuyo código es el 2111, que han sido tomados por el representante cuyo código es el 103 y que solicitan artículos del fabricante cuyo código es ACI.----------
select * from pedido where cliecod=2111 and repcod=103 and upper(fabcod)='ACI';
-------- 6 Obtener una lista de todos los pedidos ordenados por cliente y, para cada cliente, ordenados por la fecha del pedido (ascendentemente)---------
select * from pedido order by cliecod, fecha;
-------- 7 Obtener los datos de los representantes que pertenecen a la oficina de código 12 o 13 (cada representante solo pertenece a una oficina).---------
select * from repventa where ofinum in (12,13);
-------- 8 Obtener los datos de productos de los que no hay existencias o bien éstas son desconocidas.---------
select * from producto where coalesce(exist,0)=0;
-------- 9 Mostrar los representantes que fueron contratados en el 2003 ---------
select * from repventa where date_part('year', fcontrato)=2003;
-------- 10 Mostrar de los representantes el nombre y días que llevan contratados. Mostrad en una tercera columna los años que llevan contratados. Probad current_date.--------
select nombre, fcontrato, (date_part('year',current_date)-date_part('year', fcontrato))"tercera" from repventa;
----  11 Mostrar el código de los representantes que son jefe (evitando repeticiones).----
select distinct repcod from repventa where jefe is null;-
----- 12. Mostrar los nombres de los representantes que tienen jefe. -------
select nombre from repventa where jefe is not null;
----- 13. Mostrar el nombre del jefe de los representantes (sólo hay un jefe por encima de todos). -----
select select * from repventa where jefe is NULL;
------ 14. Mostrar la ciudad y región de las oficinas de la región OESTE. -------------
select ciudad, region from oficina where region='Oeste';
------ 15. Mostrar los representantes que han vendido más de lo que tenían estipulado (su cuota). Mostrad el nombre,  ventas y cuota. Ordenad descendentemente por ventas y por nombre alfabéticamente.------
select nombre, ventas, cuota from repventa where ventas > cuota order by ventas desc, nombre;
------ 16. Mostrar para cada oficina su ciudad, region, ventas, objetivo, porcentaje de ventas sobre el objetivo. Ordena por porcentaje de ventas de mayor a menor.-----
select ciudad, region, ventas, objetivo, round(ventas / objetivo * 100, 2) "porcentaje" from oficina order by porcentaje;
------ 16 bis. Idem, pero si la cuota es desconocida, en el porcentaje ha de aparecer el siguiente texto: "Desconocido" ----------
------- 17. Mostrad para cada representante su nombre, su puesto, sus ventas, su cuota y la diferencia de las ventas respecto su cuota (podrá ser positiva o negativa). Ordenad por este último campo. ------
select nombre, puesto, ventas, cuota, ventas-cuota "objectivo" from repventa order by objectivo;
-------- 18. Mostrad los representantes que han alcanzado el 50% de su cuota. Por pantalla además aparecerá el 50% de la cuota. -----------
select *, round(cuota * 0.5,2) "50 percent" from repventa where ventas/cuota > 0.5;
------- 19. Mostrar los clientes cuyo nombre empieza por A. ---------
select * from cliente where nombre like 'A%';
------- 20. Mostrar los productos cuya descripción incluya la palabra articul ----------
select * from producto where descrip like '%Articul%';
------- 21. Mostrad la descripción de los productos cuyo código de fabricante acabe con la letra I.------
select * from producto where fabcod like '%i';
------- 22. Mostrar de los representantes su nombre y oficina donde trabajan. Si alguno de ellos no la tiene asignada, se ha de mostrar "Sin asignar".  Utilizar cásting explícito de tipos de datos con cast (campo as text). Su uso es el siguiente: con la función coalesce, en lugar de poner el campo que contiene valores nulos, se ha de poner literalmente cast (campo as text), sustituyendo campo por el correspondiente.
---------
select nombre, coalesce(cast(ofinum as text), 'Sin asignar') from repventa ;
------ 23. Mostrad los pedidos que tienen importe que oscilan entre 1000 y 2000 que han sido tomandos por los siguientes representantes: 105, 106 y 108. Ordena por codigo de representante ascendentemente y por importe descendentemente. ----------
select * from pedido where importe between 1000 and 2000 and repcod in (105,106,108) order by repcod, importe desc;
------ 24. Mostrad los clientes que fueron contactados por primera vez por los representantes 101, 102 y 106 (haced el ejercicio de las dos maneras posibles). ---------
select * from cliente where repcod in (101,102,106);
select * from cliente where repcod=101 or repcod=102 or repcod=106;
------- 25. Mostrad el codigo de pedido, fecha, importe de los pedidos que fueron solicitados el año 1990 en los meses de enero, abril, julio y octubre (hacedlo de dos maneras posibles). -------
select pednum, fecha, importe from pedido where to_char(fecha,'yy')='90' and to_char(fecha,'mm') in ('01', '04', '07', '10'); 
select pednum, fecha, importe from pedido where to_char(fecha,'yyyy')='1990' and to_char(fecha,'mon') in ('jan', 'apr', 'jul', 'oct');
------ 26. Mostrar los directores de oficinas (evitando repeticiones). --------
select distinct director from oficina;
