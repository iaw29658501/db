\c scott
drop function if exists getEmpById( p_empno emp.empno%type);
create or replace function getEmpById( p_empno emp.empno%type ) 
returns varchar 
as $$
	declare v_nom emp.ename%type;
	begin
		select ename 
		into strict v_nom 
		from emp 
		where emp.empno = p_empno;
		return v_nom;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
			v_nom := 'Not Found';
			RAISE EXCEPTION 'empno % not found', p_empno;
		       	return 'not found';
	end;
$$ Language plpgsql;

select getEmpById(7369);
