\c scott
drop function if exists getEmpById( p_empno emp.empno%type);
create or replace function getEmpById( p_empno emp.empno%type ) 
returns varchar 
as $$
	declare v_emp emp%rowtype;
	begin
		select *
		into strict v_emp 
		from emp 
		where empno = p_empno;
		return v_emp.ename || ' gana ' || v_emp.sal || ' como ' || v_emp.job;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
		       		return p_empno || ' not found';
	end;
$$ Language plpgsql;

select getEmpById(7369);
select getEmpById(133337);
