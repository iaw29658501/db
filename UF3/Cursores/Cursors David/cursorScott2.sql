--() { :; }; exec psql template1 -f "$0"
/*
Nom: David Moraño Ureña
Codi: iaw47946292
*/
/*************************
-- Cursor scott 2:  --
*************************/


\c scott
drop function if exists get_emp_names2(int);


/*************************************************************
get_emp_names
**************************************************************
    Task: 
		Retorna una cadena amb una llista d'empleats.
	In:
		N/A
	Out:
		-> (text): Llista de empleats. 
*************************************************************/



CREATE OR REPLACE FUNCTION get_emp_names2()
RETURNS text
AS $$
DECLARE
	rec_emp   RECORD;
	-- A un cursor li podem passar paràmetres:
	cur_emps CURSOR(pc_deptno int)  IS               
	select e.empno id, e.ename, d.dname dept, e.job, m.ename manager
	from emp e
	    left join dept d on e.deptno = d.deptno
		left join emp m on e.mgr = m.empno
	where e.deptno = pc_deptno;	
BEGIN
	raise notice 'EMPLEATS DEPTNO #10';
	FOR rec_emp in cur_emps(10) 
	LOOP
		raise notice E'ID: %, NAME: %, JOB: %, DEPT: %, MGR: %', 
				rec_emp.id, 
				rpad(coalesce(rec_emp.ename, '---'), 8, ' '), 
				rpad(coalesce(rec_emp.job, '---'), 10, ' '),  
				rpad(coalesce(rec_emp.dept, '---'), 10, ' '),
				coalesce(rec_emp.manager, '---');
	END LOOP;
	raise notice 'EMPLEATS DEPTNO #20';
	FOR rec_emp in cur_emps(20) 
	LOOP
		raise notice E'ID: %, NAME: %, JOB: %, DEPT: %, MGR: %', 
				rec_emp.id, 
				rpad(coalesce(rec_emp.ename, '---'), 8, ' '), 
				rpad(coalesce(rec_emp.job, '---'), 10, ' '),  
				rpad(coalesce(rec_emp.dept, '---'), 10, ' '),
				coalesce(rec_emp.manager, '---');
	END LOOP;
	raise notice 'EMPLEATS DEPTNO #30';
	FOR rec_emp in cur_emps(30) 
	LOOP
		raise notice E'ID: %, NAME: %, JOB: %, DEPT: %, MGR: %', 
				rec_emp.id, 
				rpad(coalesce(rec_emp.ename, '---'), 8, ' '), 
				rpad(coalesce(rec_emp.job, '---'), 10, ' '),  
				rpad(coalesce(rec_emp.dept, '---'), 10, ' '),
				coalesce(rec_emp.manager, '---');
	END LOOP;
	raise notice 'EMPLEATS DEPTNO #40';
	FOR rec_emp in cur_emps(40) 
	LOOP
		raise notice E'ID: %, NAME: %, JOB: %, DEPT: %, MGR: %', 
				rec_emp.id, 
				rpad(coalesce(rec_emp.ename, '---'), 8, ' '), 
				rpad(coalesce(rec_emp.job, '---'), 10, ' '),  
				rpad(coalesce(rec_emp.dept, '---'), 10, ' '),
				coalesce(rec_emp.manager, '---');
	END LOOP;
	RETURN ''::text;
END;
$$ LANGUAGE plpgsql;

/*****************************
* JOC DE PROVES              *
*****************************/

select get_emp_names2();

