--iaw29658501
--Cauan Goes Mateos
\c template1
drop database if exists cauan;
create database cauan;
\c cauan;
drop function epoch2( mydate timestamp );
create or replace function epoch2( mydate timestamp ) returns int
as $$
declare t interval;
declare seconds int ;
declare minutes int ;
declare hours int;
declare dias int;
begin
t = mydate - '1970-01-01'::timestamp;
seconds = date_part('seconds', t)::int;
minutes = date_part('minutes',t)::int *60 ;
hours = date_part('seconds',t)::int * 3600 ;
dias = date_part('days',t)::int * 3600 * 24 ;
return seconds + minutes + hours + dias;
end;
$$ Language plpgsql;

select epoch2(current_timestamp::timestamp);

drop function epoch3();
create or replace function epoch3() returns int as $$
declare t int;
begin
t = epoch2(current_timestamp::timestamp);
return t;
end;
$$ Language plpgsql;

select epoch3();
