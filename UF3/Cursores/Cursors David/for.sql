--() { :; }; exec psql template1 -f "$0"
/*
Nom: David Moraño Ureña
Codi: iaw47946292
*/
/*************************
-- Cursor scott (versió for):  --
*************************/


\c scott
drop function if exists get_emp_names();


/*************************************************************
get_emp_names
**************************************************************
    Task: 
		Retorna una cadena amb una llista d'empleats.
	In:
		N/A
	Out:
		-> (text): Llista de empleats. 
*************************************************************/



CREATE OR REPLACE FUNCTION get_emp_names()
RETURNS text
AS $$
DECLARE
	emp TEXT default '';
	rec_emp   RECORD;
	cur_emps CURSOR  IS               
	select e.empno id, e.ename, d.dname dept, e.job, m.ename manager
	from emp e
	    left join dept d on e.deptno = d.deptno
		left join emp m on e.mgr = m.empno;	
BEGIN
	FOR rec_emp in cur_emps 
	LOOP
		emp := emp || char(10) || 'A' || rec_emp.id || rpad(coalesce(rec_emp.ename, '---'), 8, ' ') || 
				rpad(coalesce(rec_emp.job, '---'), 10, ' ') ||  
				rpad(coalesce(rec_emp.dept, '---'), 10, ' ')||
				coalesce(rec_emp.manager, '---') ;
	END LOOP;
	RETURN emp::text;
END;
$$ LANGUAGE plpgsql;

/*****************************
* JOC DE PROVES              *
*****************************/

select get_emp_names();

