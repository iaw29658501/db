
\c training

drop function existeixClient( p_cliecod int);
create or replace function existeixClient( p_cliecod int) returns boolean as $$ 
declare v_cliecod int;
begin
	select cliecod v_cliecod from cliente c where c.cliecod=p_cliecod;
	if found then 
		return true;
	else 
		return false;
	end if;
		-- se puede quitar el into strict y el exeption ... poniendo despues del select un
		-- if found then return true else return false;
end;
$$ language plpgsql;


