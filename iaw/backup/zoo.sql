-- Cauan Góes Mateos iaw29658501
create table Zoo (
	Codigo varchar(16) constraint zoo_cod_pk primary key,
	Nombre varchar(64),
	Ciudad varchar(32),
	Pais varchar(32),
	Tamano numeric(15,2),
	PresupuestoAnual numeric(12,2)
);

create table Especieanimal (
	IdEspecie varchar(16) constraint idespecie_pk primary key,
	NombreBulgar varchar(32),
	NombreBientifico varchar(64),
	Familia varchar(64),
	PeligrodeExtinción bit
);
create table animal (
	Codigo varchar(16) ,
	CodEspecie varchar(16),
	Sexe varchar(16),
	Especie varchar(32),
	AnoNacimiento numeric(4),
	Pais varchar(32),
	Continente varchar(16),
	constraint Codigo_FK foreign key (CodEspecie) references Especieanimal(IdEspecie),
	constraint animal_cod_codesp_pk primary key(codigo,codespecie)
	
);

