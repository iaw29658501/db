-- iaw29658501
-- Cauan Goes Mateos
-- 10/01/2019

--Exercicis de subconsultes (apartat D)


--0. Mostrar el nombre y el puesto de los que son jefe (ya está hecho con self join, ahora con subconsultas)

select ename 
from emp 
where empno in (select mgr from emp);

--1. Obtener una lista de los representantes cuyas cuotas son iguales ó superiores al objetivo de la oficina de Atlanta.

select * 
from repventa 
where cuota >= 
	(select objetivo 
		from oficina 
		where ciudad = 'Atlanta');

--2. Obtener una lista de todos los clientes (nombre) que fueron contactados por primera vez por Bill Adams.

select nombre 
from cliente 
where repcod = 
	(select repcod 
		from repventa 
		where nombre = 'Bill Adams');

--3. Obtener una lista de todos los productos del fabricante ACI cuyas existencias superan a las existencias del producto 41004 del mismo fabricante.

select * 
from producto 
where upper(fabcod) = 'ACI' 
and exist > 
	(select exist 
		from producto 
		where prodcod = '41004');

--4. Obtener una lista de los representantes que trabajan en las oficinas que han logrado superar su objetivo de ventas.

select nombre 
from repventa 
where ofinum in 
	(select ofinum 
		from oficina 
		where ventas > objetivo);

--5. Obtener una lista de los representantes que no trabajan en las oficinas dirigidas por Larry Fitch.

select * 
from repventa 
where ofinum not in 
	(select ofinum 
		from oficina 
		where director = 
			(select repcod 
				from repventa 
				where nombre = 'Larry Fitch'));

--6. Obtener una lista de todos los clientes que han solicitado pedidos del fabricante ACI entre enero y junio de 2003.

select * 
from cliente 
where cliecod in 
	(select cliecod 
		from pedido 
		where upper(fabcod) = 'ACI'
			and to_char(fecha, 'yyyy-mm') >='2003-01'
		       	and to_char(fecha, 'yyyy-mm') >='2003-06'
		);

--7. Obtener una lista de los productos de los que se ha tomado un pedido de 150 euros ó mas.

select * 
from producto 
where prodcod in 
	(select prodcod 
		from pedido
	       	where importe > 150);

--8. Obtener una lista de los clientes contactados por Sue Smith que no han solicitado pedidos con importes superiores a 18 euros.

select * 
from cliente, pedido 
where cliente.repcod = 
	(select repcod 
		from repventa 
		where nombre = 'Sue Smith') 
	and cliente.cliecod = pedido.cliecod 
	and not importe > 18;

--9. Obtener una lista de las oficinas en donde haya algún representante cuya cuota sea más del 55% del objetivo de la oficina.

select * 
from oficina 
where ofinum in 
	(select ofinum 
		from repventa 
		where cuota > objetivo/100*55
		and repventa.ofinum = oficina.ofinum);


--10. Obtener una lista de los representantes que han tomado algún pedido cuyo importe sea más del 10% de de su cuota.

select * 
from repventa 
where repcod in 
	(select repcod 
		from pedido 
		where importe > cuota/100*10
);
--11. Obtener una lista de las oficinas en las cuales el total de ventas de sus representantes han alcanzado un importe de ventas que supera el 50% del objetivo de la oficina. Mostrar también el objetivo de cada oficina (suponed que el campo ventas de oficina no existe).
select ofinum, ventas 
from oficina 
where ofinum 
in ( 

	where cuota > objetivo/2);
--12. Mostrar el nombre y ventas del representante/s contratado más recientemente.
	select nombre, ventas 
	from repventa 
	order by fcontrato desc 
	limit 5;
/*
select nombre,ventas 
from repventar
where fcontrato = 
	(select repcod 
		from pedido 
		where importe > r.cuota*10/100 );

*/
--13.1 Mostrar el director de la oficina que vende más.
select nombre 
from repventa 
where repcod = 
	(select director 
		from oficina 
		order by ventas 
		desc limit 1);
--13.2 Mostrar el director que vende más.
select nombre 
from repventa 
where puesto like '%Dir%' 
order by ventas 
desc limit 1;

--14. Lista de representantes que nunca han realizado un pedido (el campo ventas no lo tengais en cuenta).
select * 
from repventa 
where repcod 
not in (select repcod from pedido);

--15 Lista de productos que nunca han sido solicitados.
select * 
from producto 
where prodcod 
not in ( select prodcod from pedido );
--16.1. Mostrar por cliente, su gasto en la empresa (funcion de grupo). Mostar nombre y el montante de la facturacion, ordenado de mejor a peor cliente (no subconsulta).
select nombre, p.cliecod, p.importe 
from pedido p, cliente c 
where p.cliecod=c.cliecod 
group by p.cliecod, importe, nombre 
order by p.importe desc;
--16.2 Mostrar los 10 mejores clientes (no subconsulta).
select nombre, p.cliecod, p.importe 
from pedido p, cliente c 
where p.cliecod=c.cliecod 
group by p.cliecod, importe, nombre 
order by p.importe desc limit 10;
--16.3 Clientes que gastan más de la media.
select p.cliecod , round(sum(importe)/(select count(*) "media de gastos" from pedido x where x.cliecod=p.cliecod),2) from pedido p group by p.cliecod ;
--17. Mostrar por cada oficina cuál és su representante estrella (el que vende más). Mostrar código de oficina, ciudad, Nombre del representan

select o.ofinum, o.ciudad, 
		(select nombre from repventa r 
		where o.ofinum = r.ofinum 
		order by ventas limit 1) 
from oficina o;


--18. Clientes que nunca han hecho un pedido
select * 
from cliente 
where cliecod 
not in (
       	select distinct cliecod 
	from pedido 
);



