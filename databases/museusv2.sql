drop database museus;
create database museus;
\c museus
-- Versió museus amb subtipus
DROP TABLE IF EXISTS Obra;
DROP TABLE IF EXISTS Sala;
DROP TABLE IF EXISTS Museo;
DROP TABLE IF EXISTS Autor;
DROP TABLE IF EXISTS Ciudad;

CREATE TABLE Ciudad(
	idCiudad INT,
	Ciudad VARCHAR(100),
	Pais VARCHAR(150),
	CONSTRAINT ciudad_idCiudad_pk PRIMARY KEY(idCiudad)
);
CREATE TABLE Autor(
	idAutor INT,
	Nom VARCHAR(100),
	Nacionalitat VARCHAR(150),
	CONSTRAINT autor_idAutor_pk PRIMARY KEY(idAutor)
);
CREATE TABLE Museo(
	idMuseo INT,
	Nom VARCHAR(100),
	Adreca VARCHAR(150),
	idCiudad INT,
	CONSTRAINT Museo_idMuseo_fk FOREIGN KEY (idCiudad) REFERENCES Ciudad (idCiudad),
	CONSTRAINT museu_idMuseo_pk PRIMARY KEY(idMuseo)
);
CREATE TABLE Sala(
	idMuseo INT,
	idSala INT,
	CONSTRAINT Sala_idMuseo_fk FOREIGN KEY (idMuseo) REFERENCES Museo (idMuseo)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT sala_pk PRIMARY KEY(idMuseo, idSala)
);
CREATE TABLE Pintura(
	idPintura INT,
	Titol VARCHAR(100),
	idAutor INT,
	idMuseo INT,
	idSala INT,
	CONSTRAINT obra_fk FOREIGN KEY (idMuseo, idSala) REFERENCES Sala (idMuseo, idSala)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT obra_autor_fk FOREIGN KEY (idAutor) REFERENCES Autor (idAutor)
		ON DELETE RESTRICT
		ON UPDATE RESTRICT,
	CONSTRAINT obra_idPintura_pk PRIMARY KEY(idPintura)
);

CREATE TABLE Escultura(
	idEscultura INT,
	Titol VARCHAR(100),
	idAutor INT,
	idMuseo INT,
	idSala INT,
	CONSTRAINT obra_fk FOREIGN KEY (idMuseo, idSala) REFERENCES Sala (idMuseo, idSala)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT obra_autor_fk FOREIGN KEY (idAutor) REFERENCES Autor (idAutor)
		on delete RESTRICT 
		on update RESTRICT,
	CONSTRAINT obra_idEscultura_pk PRIMARY KEY(idEscultura)
);

insert into autor values (1, 'Los Gemelos', 'Brasil');
insert into autor values (2, 'Leonardo D.V.', 'Italia');
insert into ciudad values (1, 'Barcelona', 'España');
insert into museo values (1, 'Museo de la Art', 'Av Diagonal', 1);
insert into sala values (1,1);
insert into escultura values (1, 'La Virgen', 2, 1,1);
insert into pintura values (1, 'La vida', 1, 1, 1);
create or replace view obra as 
	select titol, a.nom 
		from pintura p join autor a on a.idautor=p.idautor
	 	union 
	select e.titol, a.nom 
	from escultura e join autor a on e.idautor=a.idautor;

