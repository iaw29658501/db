--() { :; }; exec psql template1 -f "$0"
-- iaw29658501
-- Cauan Goes
\c training
--Impedir que se haga un update si no han canbiado lo valores

create or replace function updater() returns trigger as $$
begin
	if NEW = OLD
		then
		raise exception 'NEW IS EQUAL TO OLD';
	end if;
	return NEW;
end;
$$ language plpgsql;

-- juego de pruevas

drop trigger tRaiseTrashUpdate on repventa;
create trigger tRaiseTrashUpdate 
before update on repventa
for each row
execute procedure updater();


