-- Cauan Goes Mateos
-- iaw29658501
-- 18/12/2018
-- exC.sql
-- 1 Mostrar la suma de las cuotas y la suma de las ventas totales de todos los representantes.
select sum(cuota) Cuota, sum(ventas) 
from repventa;
-- 2 ¿Cuál es el importe total de los pedidos tomados por Bill Adams?
select sum(p.importe) 
from pedido p, repventa r 
where p.repcod=(select repcod from repventa where nombre='Bill Adams');
-- 3  Calcula el precio medio de los productos del fabricante ACI.
select avg(precio) 
from producto 
where fabcod='aci';
--4  ¿Cuál es el importe medio de los pedido solicitados por el cliente Acme Mfg.
select avg(importe) 
from pedido p 
where p.cliecod=2103;
-- 5 Mostrar la cuota máxima y la cuota mínima de las cuotas de los representantes.
select max(cuota), min(cuota) 
from repventa;
--6  ¿Cuál es la fecha del pedido más antiguo que se tiene registrado?
select min(fecha) 
from pedido;
--7 ¿Cuál es el mejor rendimiento de ventas de todos los representantes? (considerarlo como el porcentaje de ventas sobre la cuota).
select max(ventas/cuota * 100) 
from repventa ;
--8 ¿Cuántos clientes tiene la empresa?
select count(*) 
from cliente ;
--9 ¿Cuántos representantes han obtenido un importe de ventas superior a su propia cuota?
select count(*) 
from repventa 
where ventas > coalesce(cuota,0);
-- 10 ¿Cuántos pedidos se han tomado de más de 150 euros?
select count(*) 
from pedido 
where importe > 150;
--11 Halla el número total de pedidos, el importe medio, el importe total de los mismos.
select count(pednum) "Cuantidad de pedidos", 
avg(importe) "media", sum(importe) total 
from pedido ;
--12 ¿Cuántos puestos de trabajo diferentes hay en la empresa?
select count(distinct puesto) "Cuantidad de distintos puestos de trabajo" 
from repventa ;
-- 13 ¿Cuántas oficinas de ventas tienen representantes que superan sus propias cuotas?
select count(distinct o.ofinum) 
from oficina o,repventa r 
where r.ofinum=o.ofinum 
and r.cuota < r.ventas;
-- 14 ¿Cuál es el importe medio de los pedidos tomados por cada representante?
select avg(importe), repcod 
from pedido 
group by repcod;
-- 15 ¿Cuál es el rango de las cuotas de los representantes asignados a cada oficina (mínimo y máximo)?
select ofinum, min(cuota), max(cuota) 
from repventa 
where ofinum is not null 
group by ofinum;
-- 16 ¿Cuántos representantes hay asignados a cada oficina? Mostrar Ciudad y número de representantes.
select o.ciudad, count(*) 
from oficina o, repventa r 
where r.ofinum=o.ofinum 
group by o.ciudad;
-- 17  ¿Cuántos clientes ha contactado por primera vez cada representante? Mostrar el código de representante, nombre y número de clientes.
select cliente.repcod, repventa.nombre, count(cliente.repcod) "clientes" 
from cliente, repventa 
where repventa.repcod=cliente.repcod 
group by 1,2;
-- 18  Calcula el total del importe de los pedidos solicitados por cada cliente a cada representante.
select c.nombre "cliente", r.nombre "representante", sum(p.importe) 
from cliente c, repventa r, pedido p 
where c.cliecod=p.cliecod 
and r.repcod=p.repcod 
group by 1,2;
-- 19Lista el importe total de los pedidos tomados por cada representante.
select repcod, sum(importe) 
from pedido 
group by 1;
--20
having 




