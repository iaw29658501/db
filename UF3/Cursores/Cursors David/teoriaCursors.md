# Teoria cursors
> Nom: David Moraño Ureña  
> Codi: iaw47946292

## Definició

Un cursor és una zona de memòria 

## Tipus de cursors

* **Implícits:** Declarats per el SGBD per a totes les sentències DML i PL/SQL select.
* **Explícits:** Declarats i nomenats per el programador.

## Operacions bàsiques de cursors:

* **Declare:** Crea un àrea SQL específica.
* **Open:** Identifica el joc actiu.
* **Fetch:** Carrega fila actual en variables.
* **Close:** Allibera el joc actiu.

Entre fetch i close normalment es fa una comprovació per a comprovar si hi han més files a carregar, de forma que mentre hi hagin files es continui executant fetch.

### Declaració del cursor

La sintaxi és la següent:
```
CURSOR cursor_name IS select_stmt;

cursor_name CURSOR FOR select_stmt;
```

* La sentència SELECT no contindrà clàusula INTO a la declaració d'un cursor.
* Si és necessari processar les files en una seqüencia en la que l'ordre importa, utilitzar clàusula ORDER BY.

Un exemple:
```
...

DECLARE 
	CURSOR c1 IS
		SELECT empno, ename, job, sal
		FROM emp
		WHERE sal > 2000;

	CURSOR c2 RETURN dept%ROWTYPE IS
		SELECT *
		FROM dept
		WHERE deptno = 10;
BEGIN
...
```

### Obertura del cursor

```
OPEN cursor_name;
```
(completar)

### Recuperar dades del cursor

```
FETCH cursor_name into [v1, v2, ... | record>_name];
```

(completar)

```
OPEN c_emp
LOOP
	FETCH c_emp into v_emp
	-- Si no s'han obtingut dades, el bucle acaba aquí
	EXIT WHEN v_emp is null
	-- Processament de dades a partir d'aquí
END;
```

### Tancament del cursor

```
CLOSE cursor_name;
```

* És necessari tancar el cursor un cop completat un processament de files.
* Les dades han de recuperar-se mentre el cursor sigui obert, no funcionarà si el cursor és tancat.
* Un cursor tancat es pot tornar a obrir per a realitzar un altre processament.

## Atributs d'un cursor
* **%ISOPEN** (boolean)**:** 
