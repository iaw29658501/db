--() { :; }; exec psql template1 -f "$0"
-- iaw29658501
-- Cauan Goes Mateos

create or replace function grupoCompleto(p_codasig modulo.idmodulo%type, p_codgrupo grupo.codgrupo%type) returns boolean 
as $$
declare matriculas int;
declare capacitat grupo.capacidad%type;
begin

	-- if not found ??? so dont use into strict
	select capacidad
	into capacitat
	from grupo
	where codgrupo=p_codgrupo;
	
	select count(*) 
	into matriculas
	from expediente 
	where idmodulo=upper(p_codasig)
	and codgrupo=p_codgrupo;	
	-- I need to return a boolean if not found
	if NOT FOUND then
		return false;
	end if;
/* if into strict use exception when
	EXCEPTION 
		WHEN NO_DATA_FOUND
		THEN
			RAISE EXCEPTION E'MODULO: %    O GRUPO: % NO EXITEN', p_codasig, p_codgrupo;
*/
	return capacitat <= matriculas;

end;
$$ language plpgsql;


-- EL alumno necesita de una nota igual o maior que 5 para estar aprobado verdad ?

create or replace function estaAprobada(p_codasig modulo.idmodulo%type , p_dni alumno.dni%type) returns boolean
as $$
declare
	aprobado boolean;
begin
	select coalesce(nota,0) >= 5  
	into strict aprobado
		from expediente 
		where dni=upper(p_dni) and idmodulo=upper(p_codasig) 
		order by convocatoria desc limit 1;
	return aprobado;
	exception
		when no_data_found then
			return false;
	-- Matrícula no existe
--		raise notice E'El alumno con DNI=''%'' no está matriculado en ''%''.', p_dni, p_codasig; 
end;
$$ language plpgsql;


create or replace function asignaturasPendientes(p_codasig modulo.idmodulo%type, p_dni alumno.dni%type)
returns boolean 
as $$
declare rec_prer RECORD;
declare cur_prer CURSOR is select idprerre 
				from prerrequisito 
				where idmodulo=p_codasig;	
begin
	for rec_prer in cur_prer 
	loop
		
		if not estaAprobada( rec_prer.idprerre, p_dni) then
			return false;
		end if;
	end loop;
	return true;
end;
$$ language plpgsql;


/*
create or replace function matriculaAsignatura(p_dni alumno.dni%type, p_codasig modulo.idmodulo%type, p_codgrupo grupo.codgrupo%type, p_cursoacad expediente.cursoacad%type) returns 
*/

create or replace function fMatricula() returns trigger as $$
declare v_precio modulo.precio%type;
begin
	select precio into strict v_precio
	from modulo
	where idmodulo = NEW.idmodulo;
	-- si a existe una matricula deste alumno para este año no hace falta crearla
	if ( select count(*) from matricula where dni=NEW.dni and cursoacad=NEW.cursoacad) >= 1
	then	
		update matricula 
		set importe = coalesce(importe,0) + v_precio
		where dni=NEW.dni;
	else 
		insert into matricula 
		values ( nextval('seq_nummatri'), NEW.dni, NEW.cursoacad, v_precio); 
	end if;
	RETURN NEW;
end;
$$ language plpgsql;


drop trigger tMatricula on expediente;
create trigger tMatricula
after insert on expediente
for each row
execute procedure fMatricula();






