--iaw29658501
--Cauan Goes Mateos
drop database if exists cauan;
create database cauan;
\c cauan;
drop function epoch();
create or replace function epoch() returns int
as $$

declare t interval;
declare seconds int ;
declare minutes int ;
declare hours int;
declare dias int;
begin
t = current_timestamp - '1970-01-01'::timestamp;
seconds = date_part('seconds', t)::int;
minutes = date_part('minutes',t)::int *60 ;
hours = date_part('seconds',t)::int * 3600 ;
dias = date_part('days',t)::int * 3600 * 24 ;
return seconds + minutes + hours + dias;
end;
$$ Language plpgsql;

select epoch();

