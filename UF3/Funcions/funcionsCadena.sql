coalesce
current_date
current_timestamp

--1  BBDD Scott: Fes una consulta per treure del camp ename sense cap accentuació. S'han de contemplar totes les possibilitats : accents, dièresi, caràcters especials ç, ñ, majúscules i minúscules.
select translate(ename,'áéíóúçñàèòÁÉÍÓÚÇÑÀÈÒ','aeioucnaeoAEIOUCNAEO') from emp;

-- 2 BBDD TRAINING: Es demana una sentència SQL per a crear i carregar una taula que es dirà emp2 on, a més a més dels camps propis de la taula, tindrem el camp login ( que serà la inicial del nom i el cognom.)
-- a  Per a cada representant, mostrar el camp  nombre i la posició on comença el cognom
--select nombre, position( ' ' in nombre) + 1 "Posicion Apellido" from repventa;

-- b. Per cada representant, mostra el el camp nombre (tal com està a la base de dades)  i la columna Apellido
--select nombre, split_part(nombre, ' ', 2) "Apellido" from repventa;

-- c Per cada representant, mostra el camp nombre i el seu login
--select nombre, substring(nombre from 1 for 1) || split_part(nombre , ' ', 2) from repventa;
-- d  Ja podem fer l'exercici complet!

create table emp2 as select *, split_part(nombre, ' ', 2) "Login"  from repventa;
