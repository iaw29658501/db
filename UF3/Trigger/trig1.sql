create or replace function pedido2ventas() returns trigger as $$
begin
	update repventa set ventas = ventas + NEW.importe where repcod = NEW.repcod;	
	return null;
end;
$$ language plpgsql;

DROP TRIGGER if exists trigger_pedidos2ventas on pedido ;
CREATE TRIGGER trigger_pedidos2ventas
AFTER INSERT ON pedido 
FOR EACH ROW execute procedure pedido2ventas();


