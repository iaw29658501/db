
\c training

drop function existeixClient( p_cliecod int);
create or replace function existeixClient( p_cliecod int) returns boolean as $$ 
declare v_cliecod int;
begin
	select cliecod INTO STRICT v_cliecod from cliente c where c.cliecod=p_cliecod;
		return true;
		-- se puede quitar el into strict y el exeption ... poniendo despues del select un
		-- if found then return true else return false;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			return false;
end;
$$ language plpgsql;


