-- iaw29658501 Cauan Goes

create or replace view cap (codi, nom, oficina, representats) as 
select r.repcod, r.nombre, r.ofinum, count(ree.nombre) 
from repventa r full 
join repventa ree on ree.jefe=r.repcod 
where r.repcod in 
	(select coalesce(jefe) 
	from repventa ) 
group by r.repcod
;
