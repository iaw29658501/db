-- Cauan Goes Mateos
-- iaw29658501



\c training


create sequence if not exists cliecod_seq owned by cliente.cliecod;

select setval('cliecod_seq', (select max(cliecod) from cliente), true);

create sequence if not exists pednum_seq owned by pedido.pednum;

select setval('pednum_seq', (select max(pednum) from pedido), true);


drop function existeixClient(p_id cliente.cliecod%type );
create or replace function existeixClient( p_id cliente.cliecod%type ) 
returns boolean as $$
	declare v_b smallint;
	begin
		select cliecod into strict v_b from cliente where cliecod = p_id;
		return true;
		exception
		when NO_DATA_FOUND then
			return false;
		END;
$$
LANGUAGE plpgsql;

select existeixClient(cast (2111 as smallint));


create or replace function altaClient(p_nombre varchar, p_repcod int, p_limcred cliente.limcred%type)
returns varchar
as $$
	begin
			insert into cliente(cliecod , nombre, repcod, limcred)
			values
			((select nextval('cliecod_seq')) , p_nombre, p_repcod, p_limcred);
			return 'Cleint '|| (select currval('cliecod_seq')) || ' afegit amb exit';
	end;
$$ language plpgsql;

--Comprovacion
select altaclient('Cau SL', (select repcod from cliente limit 1) , 37600);


create or replace function stockOk( p_cant int, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type ) returns boolean as $$
declare v_vassura producto.fabcod%type;
	begin
		select 'a' into strict v_vassura from producto where fabcod||prodcod=p_fabcod||p_prodcod and exist >= p_cant;
		return true;
		exception
			when NO_DATA_FOUND then
				return false;
	end;
$$
language plpgsql;

select stockOk( 1, (select fabcod from producto where exist>0 limit 1) , (select prodcod from producto where exist>0 limit 1));


create or replace function altaComanda ( p_cliecod pedido.cliecod%type , p_repcod pedido.repcod%type, p_fabcod pedido.fabcod%type, p_prodcod pedido.prodcod%type, p_cant pedido.cant%type) 
returns varchar as $$
declare message varchar;
declare v_import numeric(12,2);
begin
	if not existeixClient(p_cliecod) then
		raise exception using
			message:='Client ' || p_cliecod || ' not found';
	else
		if not stockOk(p_cant, p_fabcod, p_prodcod) then
			raise exception using
				message := p_prodcod || ' dont have ' || p_cant || ' quantity (nether more) ';

			else
				-- Let discover whats the total price (import)
			select precio * p_cant 
			into strict v_import 	
			from producto 
			where (fabcod, prodcod) = (p_fabcod, p_prodcod);
				-- Añadimos el pedido	
			insert into pedido 
			values ( ( select nextval('pednum_seq')) , current_date, p_cliecod , p_repcod , p_fabcod , p_prodcod, p_cant, v_import) ;
			update producto
			set exist = (exist - p_cant)
			where (fabcod, prodcod) = (p_fabcod, p_prodcod);

			update repventa
			set ventas = (ventas + v_import)
			where repcod = p_repcod;
			message := 'EL pedido num ' || (select currval('pednum_seq')) || ' creado con correctamente';
		end if;

	end if;
return message;
end;
$$
language plpgsql;


create or replace function updateVentas ( p_repcod repventa.repcod%type, p_ventas repventa.ventas%type)
returns varchar
as $$
begin
	update repventa 
	set ventas = ventas + p_ventas 
	where repcod = p_repcod;
	update oficina 
	set ventas = ventas + p_ventas 
	where ofinum = ( select ofinum 
			from repventa 
			where repcod = p_repcod);
	return 'Actualizado correctamente';
end;
$$
language plpgsql;
	select updateVentas(105::smallint, 10000);
	select updateVentas(105,100000);
	select updateVentas(123123,123123123);
