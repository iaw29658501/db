// Cauan Goes Mateos
// iaw29658501
/*1.*/ db.students.find({gender:"H"}).pretty().count()
/*2.*/ db.students.find({gender:"M"}).count()
/*3.*/ db.students.find({birth_year:1975}).pretty().count()
/*4.*/ db.students.find({birth_year:1993, gender:"H"}).pretty().count()
/*5.*/ db.students.find({birth_year:{ $gt: 1990 }}).count()
/*6.*/ db.students.find({birth_year:{ $lte: 1990 }}).count()
/*7.*/ db.students.find({birth_year:{ $gte: 1990, $lt: 2000}}).pretty().count()
/*8.*/ db.students.find({birth_year:{$gte: 1980, $lt: 1990 }}).pretty().count()
/*9. */db.students.find({birth_year:{ $gte: 1990, $lt: 2000}, gender:"M"}).pretty().count() 
/*10.*/ db.students.find({birth_year:{ $gte: 1980, $lt: 1990}, gender:"H"}).pretty().count()
/*11.*/ db.students.find({birth_year: { $ne: 1985 }}).pretty().count()
/*12.*/ db.students.find( {$and : [ {birth_year:1970 }, {birth_year:1980}, {birth_year:1990} ] } ).pretty().count()
/*13.*/ db.students.find({ birth_year : {$ne : [ 1970 , 1980, 1990 ] }} ).count()
/*14.*/ db.students.find( { birth_year: { $mod : [2,0 ]  } }  ).pretty().count()
/*15.*/ db.students.find( { birth_year: { $mod : [2,1 ]  } }  ).pretty().count()
/*16.*/ db.students.find( { birth_year: { $mod : [2,0 ] , $gte: 1970, $lt : 1980 }, /gender:"H" }  ).pretty().count()
/*17.*/ db.students.find( { "phone_aux": { $exists: true } }).pretty().count()
/*18*/  db.students.find( { "phone_aux": { $eq: null } }).pretty().count()
/*19*/ db.students.find({ lastname2 : { $eq: null } } ).count()
/*20*/ db.students.find( { "phone_aux": { $exists: true } , lastname2: { $eq:null } } ).pretty().count()
/*21 el save se comporta como un update caso el documento con este _id ya exista y si todavia no existe se comporta como un insert*/ 
/*22*/ //ultilizamos expresiones regulares con /ExpReg/ o passando al paremtro $regex
/*23*/ db.students.find({email:/\.net$/}}).count()
/*24*/ db.stidents.find({email:/\.org$/}).count()
/*25*/ db.students.find({ $or : [ {phone:/^662/i}, {phone_aux:/^662/}]}).pretty().count()
/*26*/   db.students.find({email:/^[0-9].*[0-9]$/}).pretty().count()
/*27*/ db.students.find({name:/^[aeiou]/i}).pretty().count()
/*28*/ db.students.find({name : / / }).pretty().count()
/*29*/ db.students.find({name: { $exists: true} , $expr : { $gte : [{ $strLenCP : "$name"} , 13] }  }).pretty().count() 
/*30*/  db.students.find({ name : /[aeiou]{3}/i  }).pretty().count()

 

