# iaw29658501

-- --------------------------------------------
##   Funcions de transaccions
-- --------------------------------------------
```
* 1.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

INSERT INTO punts (id, valor) VALUES (10,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 10;
ROLLBACK;
SELECT valor FROM punts WHERE id = 10;
```
los cambios no tienen efecto y retorna un valor de 5


Cambiara el valor para 4 donde el id es 10 pero con el rollback volveremos la situacion como estava antes del begin
```
* 2.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

INSERT INTO punts (id, valor) VALUES (20,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 20;
COMMIT;
SELECT valor FROM punts WHERE id = 20;
```
los cambios tendran efecto por el commit y el valor sera de 4
```
* 3.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

INSERT INTO punts (id, valor) VALUES (30,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (31,7);
ROLLBACK;
SELECT valor FROM punts WHERE id = 30;
```
los cambios no tendran efecto y el retorno sera de 5 por el rollback
```
* 4.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (40,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*) FROM punts;
```
primero se deleta todo de la table punts, se inserta un id y valor de (40,5) , se hace un begin, cambia el valor del q tiene id 40, hace un save point a, inserta mas valor, pero termina haciendo un rollback para el savepoint a lo que hace que el select diga que solo se tiene un valor en la tabla ahora
```
* 5.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

INSERT INTO punts (id, valor) VALUES (50,5);
BEGIN;
SELECT id, valor WHERE punts;
UPDATE punts SET valor = 4 WHERE id = 50;
COMMIT;
SELECT valor FROM punts WHERE id = 50;
```
se inicia poniendo un valor a mad de (id,valor) = (50,5)
se inicia una transacion con begin, se intenta un select sin from,
se hace un update en punts para donde el id sea igual a 50 se poner un valor igual a 4, pero no se hace puers se esperaba por el final de transacion (???) se salva con un commit y se seleciona el valor donde el id es 50 que ahora sera igual a 5

```
* 6.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (60,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor) VALUES (61,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor) FROM punts;
```
se deleta todo de la tabla punts, se pone un {valor,id} de 60 y 5 respectivamente, se inicia una transacion, en seguida un update para tener un valor de 4 donde el id es 60, se hace un savepoint a, seguido de otro insert pero con (id,valor) de (61,8), se hace un savepoint b, otro insert con (id,valor) de (61,9), un rollback para b, commit y un select de la suma de los valor que retorna 12
