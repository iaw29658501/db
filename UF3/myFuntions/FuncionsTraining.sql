--- Cauan Goes Mateos iaw29658501
--- Exercicis de Funcions per BD training
\c training

drop function if exists existeixClient(p_cliecod cliente.cliecod%type);
drop function if exists altaClient(p_nombre cliente.nombre%type, p_repcod cliente.repcod%type, p_limcred cliente.limcred%type);
drop function if exists stockOk (p_cant int, p_fabcod varchar, p_prodcod varchar);
drop function if exists altaComanda(p_fecha date, p_cliecod smallint, p_repcod smallint, p_fabcod varchar[3], p_prodcod varchar[5], p_cant smallint);

create sequence cliecod_seq;
select setval('cliecod_seq', max(cliecod), true);

create sequence pednum_seq;
select setval('pednum_seq', max(pednum), true);

--- Caldrà veure si cal crear seqüències per les claus primàries.

--- Per crear una seqüència per una clau primària d'una taula, la podeu inicialitzar a 1. Però com que en el nostre cas ja tenim dades a les taules, és imprescindible fer a continuació el següent (us poso exemple de la taula client):

--- select setval('cliecod_seq', (select max(cliecod) from cliente), true);

--- Aquesta sentència farà que la propera vegada que demanem el següent valor de la seqüència torni max+1, ja que tenim com a tercer paràmetre true sequence functions

--- Nota: Les seqúències s'han de crear obligatòriament fora de les funcions
--- Funció 	existeixClient
--- Paràmetres 	p_cliecod
--- Tasca 	comprova si existeix el client passat com argument
--- Retorna 	booleà
create or replace function existeixClient(p_cliecod cliente.cliecod%type)

returns boolean as $$
		declare
			v_cliecod smallint;
		begin
			select cliecod
			into strict v_cliecod
			from cliente
			where cliecod = p_cliecod;
			return true;
			
			exception
				when NO_DATA_FOUND then
					return false;
		end;
$$ language plpgsql;

select existeixClient(cast(2111 as smallint));
select existeixClient(cast(1234 as smallint));

--- Funció 	altaClient
--- Paràmetres 	p_nombre ,p_repcod, p_limcred
--- Tasca 	Donarà d’alta un client
--- Retorna 	Missatge Client X s’ha donat d’alta correctament
--- Nota 	si no està creada, creem una seqüència per donar valors a la clau primària. Començarà en el següent valor que hi hagi a la base de dades.
create or replace function altaClient(p_nombre cliente.nombre%type, p_repcod cliente.repcod%type, p_limcred cliente.limcred%type)
returns varchar as $$
		begin
			insert into cliente(cliecod, nombre, repcod, limcred)
			values
			(nextval('cliecod_seq'), p_nombre, p_repcod, p_limcred);
			return 'Client ' || (select last_value from cliecod_seq) || ' afegit amb exit';
		end
$$ language plpgsql;

--- Funció 	stockOk
--- Paràmetres 	p_cant , p_fabcod,p_prodcod
--- Tasca 	Comprova que hi ha prou existències del producte demanat.
--- Retorna 	booleà
create or replace function stockOk (p_cant int, p_fabcod varchar, p_prodcod varchar)
returns boolean as $$
		declare
			basura varchar[1];
		begin
			select 'a'
			into strict basura
			from producto
			where (fabcod, prodcod) = (p_fabcod, p_prodcod) and exist >= p_cant;
			return true;
			
			exception 
				when NO_DATA_FOUND then
					return false;
		end;
$$ language plpgsql;


--- Funció 	altaComanda
--- Paràmetres 	Segons els exercicis anteriors i segons necessitat, definiu vosaltres els paràmetres mínims que necessita la funció, 
--- 			tenint en compte que cal contemplar l'opció per defecte de no posar data, amb el què agafarà la data de sistema.
--- Tasca 	Per poder donar d'alta una comanda es tindrà que comprovar que existeix el client i que hi ha prou existències.
--- 		En aquesta funció heu d'utilitzar les funcions existeixClient i stockOK (recordeu de no posar select function(... ).
--- 		Evidentment, s'haura de calcular el preu de l'import en funció del preu unitari i de la quantitat d'unitats.
create or replace function altaComanda(p_fecha date, p_cliecod smallint, p_repcod smallint, p_fabcod varchar[3], p_prodcod varchar[5], p_cant smallint)
returns varchar as $$
			declare
				v_importe numeric;
				v_pednum int;
				message varchar;
			begin
				--- Comprobar si existe el cliente
				if not existeixClient(p_cliecod) then
					raise exception using
						message := ('Cliente ' || p_cliecod || ' no existeix');
				else		
				--- Comprobar si hay suficiente stock para hacer el pedido
					if not stockOk(p_cant, p_fabcod, p_prodcod) then
						raise exception using
							message := ('No hay suficiente stock para hacer el pedido');
					else		
					
					--- Calculamos el importe
						select precio * p_cant
						into strict v_importe
						from producto
						where (fabcod, prodcod) = (p_fabcod, p_prodcod);
					
						--- Añadimos el pedido
						insert into pedido(pednum, fecha, cliecod, repcod, fabcod, prodcod, cant, importe)
						values
						(nextval('pednum_seq'), p_fecha, p_cliecod, p_repcod, p_fabcod, p_prodcod, p_cant, v_importe);
				
						update producto
						set exist = (exist - p_cant)
						where (fabcod, prodcod) = (p_fabcod, p_prodcod);
				
						update repventa
						set ventas = (ventas + v_importe)
						where repcod = p_repcod;
				
						message := 'El pedido numero ' || (select last_value from pednum_seq) || ' creado correctamente';
				
				end if;
				
			end if;
				
			return message;
		end;
$$ language plpgsql;





