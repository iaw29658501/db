create table departamento(
	Numdepartamento numeric(9) constraint numdepartamento_pf primary key,
	nombre varchar(32),
	tareas varchar(64)
);

create table proyecto(
	codigo varchar(9) constraint proyecto_cod_pk primary key,
	titulo varchar(32),
	destimada varchar(32),
	dreal varchar(32),
	presupuesto numeric(15,2),
	dni varchar(9)
);

create table empleado(
	dni varchar(9) constraint empleado_dni_pk primary key, 
	nombre varchar(32),
	direccion varchar(50),
	telefono varchar(9),
	proyecto varchar(50),
	numdepartamento numeric(9),
	jefe varchar(24),
	codigo varchar(9),
	constraint jefe_dni_fk foreign key(jefe) references empleado(dni),
	constraint codigo_proyecto_fk foreign key(codigo) references proyecto(codigo)
);

alter table proyecto add constraint proyecto_dni_fk foreign key(dni) references empleado(dni);
