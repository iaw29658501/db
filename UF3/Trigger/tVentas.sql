-- Cauan Goes Mateos
-- iaw29658501

create or replace function tVentas() returns trigger as $tVentas$
declare
begin
	update repventa 
	set ventas = ventas + NEW.importe 
	where repcod = NEW.repcod;

	update oficina
	set ventas = ventas + NEW.importe
	where ofinum = ( select ofinum from repventa where repcod = NEW.repcod );

	update producto
	set exist = exist - NEW.cant
	where (fabcod, prodcod) = (NEW.fabcod, NEW.prodcod);	
	return NEW;
end;
$tVentas$ language plpgsql;

drop trigger tVentas ON pedido;
create trigger tVentas
after insert on pedido
for each row
execute procedure tVentas();

insert into pedido values ( nextval('pednum_seq'), current_date, 2117, 106, 'rei', '2a44l' , 7, 31500.00 );
