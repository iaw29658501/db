# FUNCIONS CADENA
Concatenació de cadenes:
```
string || string
```
Longitud de cadena:
```
char_length(string)
char_length('jose') => 4
```
Convertir una string a lower case:
```
lower(string)
```
Convertir una string a upper case:
```
upper(string)
```
Substring
```
substr(cadena, on comença, quants caràcters volem)
IMPORTANT: si no especifiquem quants caràcters volem, l'interpretara fins a final de la cadena.
```
Localització de la substring especificada:
```
strpos(string, substring)
IMPORTANT: s'ha de sumar 1 a la posició
```
# ROWTYPE
Una variable de tipus compost s'anomena variable **row**. Aquesta variable conté tota la fila resultant de un SELECT o un FOR. Per accedir a les columnes individualment utilitzem la notació punt(.):
```
rowvar.field
```
Per exemple:
```
CREATE FUNCTION merge_fields(t_row table1) RETURNS text AS $$
DECLARE
    t2_row table2%ROWTYPE;
BEGIN
    SELECT * INTO t2_row FROM table2 WHERE ... ;
    RETURN t_row.f1 || t2_row.f3 || t_row.f5 || t2_row.f7;
END;
$$ LANGUAGE plpgsql;

SELECT merge_fields(t.*) FROM table1 t WHERE ... ;
```

# ASSIGNACIÓ
Per asignar un valor a una variable utilitzem:
```
variable := expression;
```

# EXECUTANT UNA CONSULTA AMB UN SOL RESULTAT
El resultat d'una comanda SQL pot ser assignat a una variable record, rowtype, o una lista de variables escalables. Aixó es fa escrivint la comanda base d'SQL i afegint **INTO**. Per exemple:
```
SELECT select_expressions INTO [STRICT] target FROM ...;
INSERT ... RETURNING expressions INTO [STRICT] target;
UPDATE ... RETURNING expressions INTO [STRICT] target;
DELETE ... RETURNING expressions INTO [STRICT] target;
```
Si no s'especifica ***STRICT***, l'objectiu s'aplicarà a la primera fila que retorni o null si la consulta retorna 0 files.

Per comprobar si una consulta retorna una fila podem utilitzar ***FOUND***.
```
SELECT * INTO myrec FROM emp WHERE empname = myname;
IF NOT FOUND THEN
    RAISE EXCEPTION 'employee % not found', myname;
END IF;
```

Si s'especifica l'opció ***STRICT***, la consulta ha de retornar exactament una fila, sino apareixerà un error, aunque sigui ***NO_DATA_FOUND (no rows)*** o ***TOO_MANY_ROWS(more than one row)***. Podem utilitzar ***exception*** si volem coneixer l'error.
```
BEGIN
    SELECT * INTO STRICT myrec FROM emp WHERE empname = myname;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE EXCEPTION 'employee % not found', myname;
        WHEN TOO_MANY_ROWS THEN
            RAISE EXCEPTION 'employee % not unique', myname;
END;
```
L'execució d'una consulta ***amb STRICT*** sempre posa FOUND a true.

IMPORTANT
```
into strict + exception
into + found/not found
```

# NO FER RES
A vegades un marcador que no fa res es util. Pot indicar que un if/then/else esta BUIDA. Per exemple:
```
BEGIN
    y := x / 0;
EXCEPTION
    WHEN division_by_zero THEN
        NULL;  -- ignore the error
END;
```

# CONVERSIÓ DE TIPUS DE DADES (CASTING)
```
CAST(expression AS type);
```
Per exemple:
```
SELECT CAST('100' AS INTEGER);
```
Si no es pot fer la conversió PostgreSQL enviarà un error.
```
[Err] ERROR:  invalid input syntax for integer: "10C"
LINE 2:  CAST ('10C' AS INTEGER);
```
Castejar una string a data:
```
select CAST('2015-01-01' AS DATE);
select CAST('01-OCT-2015' AS DATE);
```
First, we converted ```2015-01-01``` literal string into ```January 1st 2015```. Second, we converted ```01-OCT-2015``` to ```October 1st 2015```.
Castejar a double:
```
SELECT
CAST ('10.2' AS DOUBLE);

Whoops, we got the following error message:

[Err] ERROR:  type "double" does not exist
LINE 2:  CAST ('10.2' AS DOUBLE)

To fix this, you need to use DOUBLE PRECISION instead of DOUBLE as follows;

SELECT
CAST ('10.2' AS DOUBLE PRECISION);
```
### Una altre forma de CAST
```
expression::type
```
Per exemple:
```
SELECT '100'::INTEGER;

SELECT '01-OCT-2015'::DATE;
```


# CONDICIONALS
Permet executar comandes alternatives basades en condicions. Tenim tres formes de ***IF***:
```
* IF ... THEN ... END IF
* IF ... THEN ... ELSE ... END IF
* IF ... THEN ... ELSIF ... THEN ... ELSE ... END IF
```
I dos formes de ***CASE***:
```
* CASE ... WHEN ... THEN ... ELSE ... END CASE
* CASE WHEN ... THEN ... ELSE ... END CASE
```

### IF-THEN
```
IF boolean_expression THEN
  statements
END IF;
```
Per exemple:
```
IF v_user_id <> 0 THEN
  UPDATE users SET email = v_email WHERE user_id = v_user_id;
END IF;
```
### IF-THEN-ELSE
```
IF boolean-expression THEN
  statements
ELSE
  statements
END IF;
```
Per exemple:
```
IF v_count > 0 THEN
  INSERT INTO users_count (count) VALUES (v_count);
  RETURN 't';
ELSE
  RETURN 'f';
END IF;
```
### IF-THEN-ELSIF
```
IF boolean-expression THEN
    statements
[ ELSIF boolean-expression THEN
    statements
[ ELSIF boolean-expression THEN
    statements
    ...]]
[ ELSE
    statements ]
END IF;
```
Per exemple:
```
IF number = 0 THEN
    result := 'zero';
ELSIF number > 0 THEN
    result := 'positive';
ELSIF number < 0 THEN
    result := 'negative';
ELSE
    -- hmm, the only other possibility is that number is null
    result := 'NULL';
END IF;
```
### SIMPLE CASE
```
CASE search-expression
    WHEN expression [, expression [ ... ]] THEN
      statements
  [ WHEN expression [, expression [ ... ]] THEN
      statements
    ... ]
  [ ELSE
      statements ]
END CASE;
```
Per exemple:
```
CASE x
    WHEN 1, 2 THEN
        msg := 'one or two';
    ELSE
        msg := 'other value than one or two';
END CASE;
```

### SEARCHED CASE
```
CASE
    WHEN boolean-expression THEN
      statements
  [ WHEN boolean-expression THEN
      statements
    ... ]
  [ ELSE
      statements ]
END CASE;
```
Per exemple:
```
CASE
    WHEN x BETWEEN 0 AND 10 THEN
        msg := 'value is between zero and ten';
    WHEN x BETWEEN 11 AND 20 THEN
        msg := 'value is between eleven and twenty';
END CASE;
```

### BUCLES SIMPLES
#### LOOP
Loop defineix un bucle incondicional que es repeteix fins que termina fins un ***EXIT*** o ***RETURN***.
```
[ <<label>> ]
LOOP
    statements
END LOOP [ label ];
```
#### LOOP EXIT
```
LOOP
    -- some computations
    IF count > 0 THEN
        EXIT;  -- exit loop
    END IF;
END LOOP;

LOOP
    -- some computations
    EXIT WHEN count > 0;  -- same result as previous example
END LOOP;

<<ablock>>
BEGIN
    -- some computations
    IF stocks > 100000 THEN
        EXIT ablock;  -- causes exit from the BEGIN block
    END IF;
    -- computations here will be skipped when stocks > 100000
END;
```
#### LOOP CONTINUE
```
LOOP
    -- some computations
    EXIT WHEN count > 100;
    CONTINUE WHEN count < 50;
    -- some computations for count IN [50 .. 100]
END LOOP;
```

#### WHILE
Repeteix una secuencia de declaracions mentre la expressió boleana sigui verdadera.
```
WHILE amount_owed > 0 AND gift_certificate_balance > 0 LOOP
    -- some computations here
END LOOP;

WHILE NOT done LOOP
    -- some computations here
END LOOP;
```

#### FOR IN
Crea un bucle que itera sobre un rang de valors enters.
```
[ <<label>> ]
FOR name IN [ REVERSE ] expression .. expression [ BY expression ] LOOP
    statements
END LOOP [ label ];
```
Per exemple:
```
FOR i IN 1..10 LOOP
    -- i will take on the values 1,2,3,4,5,6,7,8,9,10 within the loop
END LOOP;

FOR i IN REVERSE 10..1 LOOP
    -- i will take on the values 10,9,8,7,6,5,4,3,2,1 within the loop
END LOOP;

FOR i IN REVERSE 10..1 BY 2 LOOP
    -- i will take on the values 10,8,6,4,2 within the loop
END LOOP;
```

# CURSORS  
Els pasos per fer un cursor son:  
1. **Declaració** del cursor.
2. **Obrir** el cursor.
3. **Buscar** les línies del resultat i guardarles en un objectiu.
4. **Comprobar** si hi ha més línies a buscar. Si es aquest cas, tornem al pas 3 sino passem al pas 5.
5. **Tancar** el cursor.

### Tipus de CURSORS
* Cursor ***explícit***: variables que emmagatzemen dades respecte de consultes de una o més taules, per tant, un cursor esta format per una instrucció SELECT i com tal, ha de ser declarat com una variable.
```
CREATE OR REPLACE FUNCTION expl_cursor1()
RETURNS SETOF clientes
AS $BODY$
  DECLARE
    -- Declaración EXPLICITA del cursor
    cur_clientes CURSOR FOR SELECT *
                            FROM clientes;
    registro clientes%ROWTYPE;
  BEGIN
   -- Procesa el cursor
     FOR registro IN cur_clientes LOOP
         RETURN NEXT registro;
     END LOOP;
   RETURN;
  END
$BODY$ LANGUAGE 'plpgsql'
```
* Cursor ***Implícit*** estan insertats directament en el codi com una instrucció SELECT, sense necessitat de ser declarades previament, generalment requereixen alguna variable del tipus RECORD o ROWTYPE per capturar les files.
```
CREATE OR REPLACE FUNCTION impl_cursor2()
RETURNS SETOF clientes
AS $BODY$
  DECLARE
      registro clientes%ROWTYPE;
  BEGIN
     -- Cursor IMPLICITO en el ciclo FOR
     FOR registro IN SELECT *
                      FROM clientes LOOP
         RETURN NEXT registro;
     END LOOP;
     RETURN;
  END
$BODY$ LANGUAGE 'plpgsql'
```
### Declaració del cursor.
```
DECLARE rec_film RECORD;
```
Declaració de la variable de tipus record per al cas del LOOP i el WHILE.  
Per declarar el cursor:  
```
DECLARE
  cursor_name CURSOR FOR
  select
    bloc d'instruccions...
```
Per exemple:
```
DECLARE
  cur_films CURSOR FOR SELECT * FROM film;
  cur_films2 CURSOR (year integer) FOR SELECT * FROM film WHERE release_year = year;
```
***cur_films*** és un cursor que encapsula totes les files de la taula **film**.  
***cur_films2*** és un cursor que encapsula pelicules d'un any en particular a la taula **film**.

### OBRINT UN CURSOR
Els cursors han de ser oberts abans de ser utilitzats sobre files de consulta.  
Podem ***obrir cursors sense estableixer***:  
```
OPEN my_cursor FOR
SELECT * FROM city WHERE counter = p_country;
```
Per ***obrir un cursor establegut***:  
```
OPEN cur_films;
OPEN cur_films2(year := 2005);
```

### Utilitzar els cursors
Després de obrir els cursors ya podem manipular-les utilitzant FETCH, MOVE, UPDATE o DELETE.  
Buscar(fetch) la següent fila.
```
FETCH cursor_name INTO target_variable;
```
Per exemple:  
```
LOOP
-- Lectura / Adquisició de informació.
  FETCH cur_emp INTO rec_emp;
  EXIT WHEN NOT FOUND;
  RAISE NOTICE 'Empleat % Departament: % Treball: %', rec_emp.ename, rec_emp.deptno, coalesce(rec_emp.job, 'Sense treball');
```

### Comprobar si hi ha més línies
```
EXIT WHEN NOT FOUND
```

### Tancar el cursor
```
CLOSE cursor_name
```

# Triggers

***SINTAXI***
***Definició del trigger***
```
CREATE [OR REPLACE] TRIGGER trigger_name
{BEFORE | AFTER} {DELETE | INSERT | UPDATE [OF col1, col2, ...]} (TEMPORITZACIÓ, Quan s'executa el trigger)
[OR DELETE | INSERT | UPDATE [OF col1, col2, ...]]
ON table (OBJECTE OBJECTIU(taula, vista...))
{REFERENCING OLD AS oldname, NEW as newname} (OPCIONAL)
[FOR EACH ROW [WHEN {condition}]] (Quantes vegades vull que s'executi el trigger, )
pl/sql_block
```

### Tipus de Triggers:
* Taules
* Vistes
* Esdeveniments

### Nivells de triggers:
* A nivell de fila (RETURN NEW || RETURN ALL)
* A nivell de sentencia (RETURN NULL)

### Estructura de trigger:
1. Definició del trigger (capçalera)
2. Funció del trigger (cos)

### Número de files afectades:
* 1 vegada (statement) ATENCIÓ: No podem utilitzar ***OLD*** i ***NEW***
* n vegades (row) ATENCIÓ: Utilitzem ***OLD*** i ***NEW***

***IMPORTANT***: Els triggers no tenen paràmetres, els únics que funcionaran com
a argument són **OLD** i **NEW**.

***NEW*** serverix per:
* INSERT
* UPDATE
* DELETE

***OLD*** serveix per:
* UPDATE
* DELETE

#### Tipus de Triggers

- `Statement-level triggers` (a nivell de sentència)
- `Row-level triggers` (a nivell de fila) -> OLD i NEW només es poden utilitzar a nivell de fila

`after` | `before` -> trigger taula nivell fila/sentència

`instead of` -> trigger vista

#### Funcions de Trigger

|Funció|Descripció|
|----|----|
|NEW|Data type RECORD; variable holding the new database row for INSERT/UPDATE operations in `row-level` triggers. This variable is null in statement-level triggers and for DELETE operations.|
|OLD|Data type RECORD; variable holding the old database row for UPDATE/DELETE operations in `row-level` triggers. This variable is null in statement-level triggers and for INSERT operations.|

|TG_OP|Data type text; a string of INSERT, UPDATE, DELETE, or TRUNCATE telling for which operation the trigger was fired.|


#### Us de OLD i NEW

|OP DML|NEW|OLD|
|------|----|--|
|INSERT|**SI**|NO|
|UPDATE|**SI**|**SI**|
|DELETE|NO|**SI**|
