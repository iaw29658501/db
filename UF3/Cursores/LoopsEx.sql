--- iaw49185512		Erik Perdigones Garcia

--- Ejercicio con el cursor en tabla scott

\c scott

CREATE OR REPLACE FUNCTION get_empleats()
RETURNS text
AS $$
DECLARE
 nombres TEXT DEFAULT '';
    -- declarem una variable fila anomenada rec_film, la qual reculirà cada fila
    -- que es llegeix (fetch) del cursor
    -- Oracle permet %rowtype per taules i cursors, postgreSQL per taules només
 rec_emp   RECORD;
    -- Els cursors poden admetre paràmetres.
    -- Aquest és un exemple de cursor bàsic, sense paràmetres
 cur_empleats CURSOR  IS               -- postgreSQL usa habitualment "FOR"
     SELECT nomemp, ofici     -- pero permet "IS" com a gest cap  a Oracle
     FROM empleat;
BEGIN
   -- Open the cursor
   OPEN cur_empleats;
   LOOP
      -- fetch row into the film
      FETCH cur_empleats INTO rec_emp;
      -- exit when no more row to fetch
      -- La sintaxi de postgreSQL difereix una
      -- mica d'Oracle, que sería així:
      -- EXIT WHEN cur_films%NOTFOUND;
      EXIT WHEN NOT FOUND;
      -- build the output
      IF rec_emp.ofici= 'CLERK' THEN
         nombres := nombres || E'\n' || rpad(rec_emp.nomemp,10,' ')  || ':' || rec_emp.ofici;
      END IF;
   END LOOP;
   -- Close the cursor
   CLOSE cur_empleats;

   RETURN nombres;
END;
$$ LANGUAGE plpgsql;


select get_empleats();

----- ESTE EJEMPLO ES CON WHILE

CREATE OR REPLACE FUNCTION get_empleats2()
RETURNS text
AS $$
DECLARE
 nombres TEXT DEFAULT '';
    -- declarem una variable fila anomenada rec_film, la qual reculirà cada fila
    -- que es llegeix (fetch) del cursor
    -- Oracle permet %rowtype per taules i cursors, postgreSQL per taules només
 rec_emp   RECORD;
    -- Els cursors poden admetre paràmetres.
    -- Aquest és un exemple de cursor bàsic, sense paràmetres
 cur_empleats CURSOR  IS               -- postgreSQL usa habitualment "FOR"
     SELECT nomemp, ofici     -- pero permet "IS" com a gest cap  a Oracle
     FROM empleat;
BEGIN
   -- Open the cursor
   OPEN cur_empleats;
   FETCH cur_empleats INTO rec_emp;
   while FOUND	
   LOOP
      IF rec_emp.ofici= 'CLERK' THEN
      --Un printf cutre
--RAISE NOTICE 'Empleat % ofici: %', rec_emp.nomemp, rec_emp.ofici;
			nombres := nombres || E'\n' || rpad(rec_emp.nomemp,10,' ')  || ':' || rec_emp.ofici;
      END IF;
      -- Comprobar si aun tiene o recibe datos del cursor
      FETCH cur_empleats INTO rec_emp;
   END LOOP;
   -- Close the cursor
   CLOSE cur_empleats;
	
   RETURN nombres;
END;
$$ LANGUAGE plpgsql;


select get_empleats2();

--- EJEMPLO CON FOR

CREATE OR REPLACE FUNCTION get_empleats3()
RETURNS text
AS $$
DECLARE
 nombres TEXT DEFAULT '';
    -- declarem una variable fila anomenada rec_film, la qual reculirà cada fila
    -- que es llegeix (fetch) del cursor
    -- Oracle permet %rowtype per taules i cursors, postgreSQL per taules només
 rec_emp   RECORD;
    -- Els cursors poden admetre paràmetres.
    -- Aquest és un exemple de cursor bàsic, sense paràmetres
 cur_empleats CURSOR  IS               -- postgreSQL usa habitualment "FOR"
     SELECT nomemp, ofici     -- pero permet "IS" com a gest cap  a Oracle
     FROM empleat;
 v_empleat text;
BEGIN
   -- Open the cursor
 
   FOR v_empleat IN cur_empleats
   LOOP 
	RAISE NOTICE 'Empleat % ofici: %', v_empleat.nomemp, v_empleat.ofici;
	
	END LOOP;
   -- Close the cursor

	
   RETURN nombres;
END;
$$ LANGUAGE plpgsql;


select get_empleats3();
