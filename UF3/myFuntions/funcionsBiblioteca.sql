<<<<<<< HEAD
-- iaw29658501 Cauan Goes Mateos

\c biblioteca
drop function if exists documentDisponible(varchar);

drop type if exists t_exemplar;


-- Donat un titol de document com a argument, torna la llista d'exemplars d'aquest disponibles per a préstec 
-- (NOTA : si existeix més d'un exemplar a,b el títol donat, tractar només el primer)

create or replace function documentDisponible(varchar p_docid) RETURNS varchar as $$
declare
begin
	select * from docuemt
end;
language plpgsql;
=======
--- iaw29658501 Cauan Goes Mateos

-- Donat un titol de document com a argument, torna la llista d'exemplars d'aquest disponibles per a prèstec
-- Si existeix més d'un exemplar amb el títol donat, tractar només el primer

\c biblioteca
drop function if exists documentDisponible(varchar);
create or replace function documentDisponible(p_titol varchar)
returns varchar
as $$
declare
	v_idDocument int;
begin
	SELECT idDocument 
	INTO strict v_idDocument
	FROM llibre
	WHERE lower(titol) = lower(p_titol)
	order by idDocument
	limit 1;
	
	
	return (SELECT idexemplar
	from prestec
	where idexemplar in
	(
	SELECT idexemplar
	from exemplar
	where iddocument = v_idDocument
	)	
	and datadev is not null
	);
	EXCEPTION 
		WHEN NO_DATA_FOUND THEN
			return p_titol || ' not found';
end;
$$ LANGUAGE plpgsql;

select documentDisponible('fundamentos de bases de datos');
select documentDisponible('xxxx');
select documentDisponible(123);
>>>>>>> 3faa108ce39e78b968c7254588605a3d24931269
