-- Cauan Góes Mateos iaw29658501
create table client(
DNI VARCHAR(9) constraint dni_cliente_pk primary key,
nom varchar(20) constraint client_nom_nn not null,
adreca varchar(50),
telefon varchar(9)
	
);

create table asunto(
numexpediente numeric(9) constraint asunto_numexp_pk primary key,
fechaInicio date constraint asunto_date_nn not null,
fechaArchivo date,
estat varchar(1),
asunto_dni varchar(9),
constraint asunto_dni_fk foreign key(asunto_dni) references client (dni),
constraint ck_estat_ot check (estat='T' or estat='O')
);

create table asuntoxProcurador(
numexpediente numeric(9),
DNI varchar(9),  
constraint numexpediente_FK foreign key (numexpediente) references asunto(numexpediente),     
constraint DNI_FK foreign key (DNI) references procurador(DNI),
constraint asuntoxProcurador_numexp_dni_pk primary key(NumExpediente,DNI)         
);

create table procurador(
DNI varchar(9) constraint dni_procurador_pk primary key,
nombre varchar(50) constraint procurador_nombre_nn not null unique,
direccion varchar(64),
telefono numeric(9),
constraint procurador_nombre_uk unique(nombre)
);

