-- iaw29658501
-- Cauan Goes Mateos

create or replace function tControl() 
returns trigger 
as $$
begin
	INSERT INTO control values ( default, current_user, current_timestamp, TG_OP );
	return NEW;
end;
$$ language plpgsql;
drop trigger tControl on emp;


DROP trigger tControl on emp;
CREATE trigger tControl
AFTER update or insert or delete ON emp
FOR EACH ROW
EXECUTE PROCEDURE tControl();

update emp 
set deptno=20 
where empno > 7900;
