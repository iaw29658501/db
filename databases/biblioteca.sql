--() { :; }; exec psql template1 -f "$0"

-- Script: 					biblioteca.sql
-- Descripció: 			Biblioteca pública on els usuaris poden agafar en prèstec
--									diferents tipus de documents (libres, CDs de musica, pel·lícules
--									i revistes.
-- Autor: 					Jordi Andúgar
-- Data creació:		20/04/2019
-- Darrera revisió:	30/04/2019
-- Versió:					0.1

drop database  if exists biblioteca;
create database biblioteca;
\c biblioteca
\x

-- Document serà un supertipus del qual heretaran 4 subtipus de documents
-- que es poden prestar:
-- 		* libre
-- 		* musica
-- 		* pel·lícula
-- 		* revista

create table document(
	idDocument serial primary key ,
	titol varchar(200),
	descripcio varchar(200)
);

/* libre serà una taula que tindrà la següent estructura:
		-- heretats
	 idDocument serial primary key ,
	 titol varchar(200),
	 descripcio varchar(200)
		--- propis
		ISBN varchar(100),
		edicio varchar(50),
		signatura varchar(10),
		revTecnica varchar(200)
*/
create table llibre(
	ISBN varchar(100),
	edicio varchar(50),
	signatura varchar(10),
	revTecnica varchar(200)
)  inherits (document);

/* musica serà una taula que tindrà la següent estructura:
		-- heretats
	idDocument serial primary key ,
	titol varchar(200),
	descripcio varchar(200)
		--- propis
	numSegell varchar(100),
	publicacio varchar(100),
	edicio varchar(50),
	signatura varchar(10),
	contingut varchar(200),
	tema int
*/

create table musica(
	numSegell varchar(100),
	publicacio varchar(100),
	edicio varchar(50),
	signatura varchar(50),
	contingut varchar(200),
	tema int
) inherits (document);

create table pelicula (
	publicacio varchar(100),
	durada smallint,
	anyRealitzacio smallint,
	classificacio varchar(100),
	nota varchar(200),
	sinopsi varchar(1000)
	/*
	codgen   smallint constraint pelicula_genere_nn not null,
  coddir   smallint constraint pelicula_director_nn not null,
	sinopsi varchar(1000),
	dataestrena  date,
  foto varchar(200),
	constraint pelicula_genere_fk foreign key (codgen)
                        references genere (codgen),
  constraint pelicula_director_fk foreign key (coddir)
                        references director (coddir)*/
) inherits (document);

create table revista(
	ISSN varchar(100),
	signatura varchar(10),
	publicacio  varchar(200),
	periodicitat varchar(50)
)  inherits (document);


-- La taula autor valdrà per escriptors, musics i directors de pelicula
create table autor (
	idAutor serial primary key,
	nom varchar(200) constraint autor_nom_uk unique
);

insert into autor
values (default,'Silberschatz, Abraham'),(default,'Korth, Henry F.'), (default,'Sudarshan, S.'),
		(default,'AC/DC'),
		(default, 'Chazelle, Damien')
;
create table actor (
	idActor serial primary key,
	nom varchar(200) constraint actor_nom_uk unique
);

create table repartiment(
	idActor int,
  idDocument int,
	constraint repartiment_pk primary key (idActor, idDocument),
	constraint repartiment_idActor_fk foreign key (idActor)
									references actor(idActor)
);

insert into actor
values
		(default, 'Gosling, Ryan'),
		(default, 'Stone, Emma'),
		(default, 'DeWitt, Rosemarie')
;


-- Fixeu-vos que no peta
insert into repartiment
values  (1,3), (2,3),(3,3)
;


/* libre tindrà la següent estructura:

		-- heretats
	 idDocument serial primary key ,
	 titol varchar(200),
	 descripcio varchar(200)
		--- propis
		ISBN varchar(100),
		edicio varchar(50),
		signatura varchar(10),
		revTecnica varchar(200)
*/
insert into llibre
values (
 default,
 'Fundamentos de bases de datos',
 'XVII, 653 p. : il. ; 28 cm',
 '9788448190330',
 '6ª ed.',
 '681.3 Sil',
 'Jesús Sánchez Allende');

 insert into musica
values (
 default,
 'Highway to hell',
 '1 disc (CD) (42 min) : estèreo + 1 fullet (15 p.)',
 '510764 2 EPIC',
 '[S.l.] : Albert Son, cop. 2003',
 'Digitally Remastered',
 'CD 2.AC/D 40',
 'Inclou accés exclusiu a la web www.acdcrocks.com a través de ConnecteD, inclòs al propi CD',
 3
);

insert into pelicula
values (
 default,
 'La La Land [enregistrament vídeo] : la ciudad de las estrellas / escrita y dirigida por Damien Chazelle',
 '1 disc òptic (BD) : col., so.',
 'Madrid : Universal, DL 2017',
 128, -- en minuts
 2016,
 'Per a tots els públics',
 'Opcions de so i subtítols en castellà i anglès',
 'Mia (Emma Stone) és una de les moltes aspirants a actriu que viuen a Los Angeles a la recerca del somni hollywoodià, es guanya la vida com a cambrera mentre es presenta a munts de proves de càsting. Sebastian (Ryan Gosling) és un pianista que viu de les actuacions de segona que li surten, i el seu somni és regentar el seu propi club on retre tribut al jazz més pur.
Les destinacions de Mia i Sebastian es creuaran i la parella descobrirà l''amor, un vincle que farà florir i després posar en escac les aspiracions de tots dos. En una competició constant per buscar un lloc en el món de l''espectacle, la parella descobrirà que l''equilibri entre l''amor i l''art pot ser el major obstacle de tots.'
);

create table premi(
 idPremi serial constraint premi_pk primary key,
 guardo varchar(200)
);

insert into premi
values (default,'Oscar'), (default,'Globus d''or'), (default,'BAFTA'), (default,'Festival de Venècia'),  (default,'Goya');

create table premiPelicula (
 idPremi int,
 idDocument int,
 quantitat smallint,
 constraint premiPelicula_pk primary key (idPremi,idDocument),
 constraint premiPelicula_idPremi_fk foreign key (idPremi) references premi(idPremi)
);

insert into premiPelicula
values (1,3,6) ,(2,3,7), (3,3,5), (4,3,1);

-- select * from only document;
-- No devolverá nada

select * from document;
-- https://www.mycodesmells.com/post/bonus---inheritance-in-postgresql
-- No es pot posar idDocument com a CA, no funciona perquè realment no existeix
-- La fila en document
create table exemplar(
	idExemplar int primary key,
	idDocument int -- no funciona: references document(idDocument)
);

insert into exemplar
values (1,1), (2,1), (3,1), (4,2), (5,2), (6,3);

create table autorDocument(
	autor int,
	document int,
	primary key(autor, document)
);

-- Taula que recull qui intervé en un document,
-- sigui llibre, musica o pel·lícula
insert into autorDocument
values (1,1), (2,1), (3,1), --llibre de BD
				(4,2),							-- CD
			(5,3), (6,3), (7,3)		-- DVD
;

create table tema(
	idTema serial primary key,
	tema varchar(200)
);
insert into tema
values (default, 'Bases de dades'),
		(default, 'Informàtica'),
		(default, 'Hard rock (Música)');

create table temaDocument(
	tema int,
	document int,
	constraint temaDocument_pk primary key(tema, document),
	constraint temaDocument_fk foreign key (tema) references tema(idTema)
);

insert into temaDocument
values (1,1), (2,1);

CREATE TABLE Usuari
       (idUsuari serial CONSTRAINT SOCI_CODSOCI_PK PRIMARY KEY ,
        LOGIN    VARCHAR(15),
        PASSWORD character varying(32),
        DNI varchar(9) ,
        NOM   varchar(15) CONSTRAINT SOCI_NOMSOCI_NN NOT NULL,
        COGNOMS varchar(30) CONSTRAINT SOCI_COGNOMS_NN NOT NULL,
    	DATA_NAIXEMENT DATE,
        ADRECA varchar(50) CONSTRAINT SOCI_DIRSOCI_NN NOT NULL,
        TELEFON varchar(9) CONSTRAINT SOCI_TELFSOCI_NN NOT NULL,
		SEXE varchar(1),
		puntsDemerit smallint,
		bloquejat date
);

INSERT INTO Usuari
VALUES
(1, 'mcapdevila',md5('manel'),'12345678Z','Manel','Capdevila',null,'SARDENYA 12-20','932982824','H',0,null),
(2,'arobles',md5('anna'),null,'Anna','Robles',null,'GRAN VIA 300','933102376','D',0,null),
(3,'jandreu',md5('julia'),null,'Julia','Andreu',null,'DIAGONAL 450','933457698','H',0,null),
(4,'aboixaderas',md5('arnau'),null,'Arnau','boixaderas',null,'VIA LAIETANA 123','933452200','H',0,null),
(5,'afernandez',md5('alexia'),null,'Alèxia','fernandez',null,'MARINA 233','933152304','D',0,null);

/*
Possibles estats del prèstec:
Disponible
Exclòs de prèstec
En exposició
Venç el : dataPres+30 dies
*/
create table prestec (
	idExemplar int,
	dataPres timestamp,
	dataDev timestamp,
	idUsuari int,
	constraint prestec_pk primary key(idExemplar,dataPres),
	constraint prestec_exemplar_fk_ foreign key(idExemplar) references exemplar(idExemplar),
	constraint prestec_usuari_fk foreign key(idUsuari) references usuari(idUsuari)
);

insert into prestec
values  (1, current_timestamp - interval '32 days',null,1),
	(4, current_timestamp - interval '10 days',null,3),
	(5, current_timestamp - interval '25 days',null,2),
	--prestecs retornats
	(1, current_timestamp - interval '50 days', current_timestamp - interval '40 days',4 )
;

