
-- 1#    Creeu el rol rol_repventa
create role rol_repventa;
-- 2 #    Assigneu privilegis de lectura sobre la taula pedido de la BBDD training
\c training;
grant select on table pedido to rol_repventa;
-- 3#    Creeu el rol rol_scott
\c scott;
create role rol_scott
-- 4#    Assigneu tots els privilegis (select, insert, update, delete) sobre totes les taules de la BBDD scott.
\c scott;
-- creando el role rol_scott con todos los permisos para la db scott
GRANT ALL PRIVILEGES ON DATABASE SCOTT TO ROL_SCOTT;
-- 5 #    Assignar els rols al company.
-- primero tengo de crear su usuario localmente
CREATE USER iaw49185512;
-- ahora puedo dar los roles para el usuario de mi company 
GRANT ROL_SCOTT, ROL_REPVENTA TO iaw49185512;
-- 6#    Per canvia de rol, heu de fer "set role nou_rol;"
set role rol_repventa;
-- 7#    Comproveu que podeu fer només el que el rol us marca
-- conecto a training y demonstro que solo se puede hacer un select en la tabla pedido pero no en repventa
\c training
select * from pedido;
select * from repventa;
-- 8#    Per tornar al vostre rol, "reset role;"
-- poniendo el role normal deste usuario
reset role;
-- 9 #    Al rol rol_scott treieu els privilegis d'update i delete.
--    Comprova TOTES les operacions (es demana totes les consultes per verificar-ho)
-- tirare todas los permisos de update y de delete del rol_scott
revoke update on all tables in schema public from rol_scott;
revoke delete on all tables in schema public from rol_scott;



-- ROLLBACK              SAVEPOINT               COMMIT
-- grant s,i,u,d  ON all tables IN schema public TO rol 


-- Els rollback deshacen desde el principio de la transacio o desde el principio de un sevepoint
