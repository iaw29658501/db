

Primero cambio lo archivo de configuracion para aceptar conexiones de la red interna
-- ident uses the os username as login
# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     peer
# IPv4 local connections:
host    all             all             127.0.0.1/32            ident
host    all             all             192.168.1.25/24         trust
# IPv6 local connections:
host    all             all             ::1/128                 ident
# Allow replication connections from localhost, by a user with the
# replication privilege.
#local   replication     postgres                                peer
#host    replication     postgres        127.0.0.1/32            ident
#host    replication     postgres        ::1/128                 ident


despues puedo hacer los ejercicios de permisos

-- iaw29658501
-- Mismo script que los demas de mi grupo
-- 1# Creeu el rol rol_repventa
create role rol_repventa;
-- 2 # Assigneu privilegis de lectura sobre la taula pedido de la BBDD training
\c training;
grant select on table pedido to rol_repventa;
-- 3# Creeu el rol rol_scott
\c scott;
create role rol_scott
-- 4# Assigneu tots els privilegis (select, insert, update, delete) sobre totes les taules de la BBDD scott.
\c scott;
-- creando el role rol_scott con todos los permisos para la db scott
GRANT ALL PRIVILEGES ON DATABASE SCOTT TO ROL_SCOTT;
-- 5 # Assignar els rols al company.
-- primero tengo de crear su usuario localmente
CREATE USER iaw49185512;
-- ahora puedo dar los roles para el usuario de mi company
GRANT ROL_SCOTT, ROL_REPVENTA TO iaw49185512;
-- 6# Per canvia de rol, heu de fer "set role nou_rol;"
set role rol_repventa;
-- 7# Comproveu que podeu fer només el que el rol us marca
-- conecto a training y demonstro que solo se puede hacer un select en la tabla pedido pero no en repventa
\c training
select * from pedido;
select * from repventa;
-- 8# Per tornar al vostre rol, "reset role;"
-- poniendo el role normal deste usuario
reset role;
-- 9 # Al rol rol_scott treieu els privilegis d'update i delete.
-- Comprova TOTES les operacions (es demana totes les consultes per verificar-ho)
-- tirare todas los permisos de update y de delete del rol_scott
revoke update on all tables in schema public from rol_scott;
revoke delete on all tables in schema public from rol_scott;




# iaw29658501

-- --------------------------------------------
##   Funcions de transaccions
-- --------------------------------------------
```
* 1.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

INSERT INTO punts (id, valor) VALUES (10,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 10;
ROLLBACK;
SELECT valor FROM punts WHERE id = 10;
```
los cambios no tienen efecto y retorna un valor de 5


Cambiara el valor para 4 donde el id es 10 pero con el rollback volveremos la situacion como estava antes del begin
```
* 2.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

INSERT INTO punts (id, valor) VALUES (20,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 20;
COMMIT;
SELECT valor FROM punts WHERE id = 20;
```
los cambios tendran efecto por el commit y el valor sera de 4
```
* 3.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

INSERT INTO punts (id, valor) VALUES (30,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (31,7);
ROLLBACK;
SELECT valor FROM punts WHERE id = 30;
```
los cambios no tendran efecto y el retorno sera de 5 por el rollback
```
* 4.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (40,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*) FROM punts;
```
primero se deleta todo de la table punts, se inserta un id y valor de (40,5) , se hace un begin, cambia el valor del q tiene id 40, hace un save point a, inserta mas valor, pero termina haciendo un rollback para el savepoint a lo que hace que el select diga que solo se tiene un valor en la tabla ahora
```
* 5.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

INSERT INTO punts (id, valor) VALUES (50,5);
BEGIN;
SELECT id, valor WHERE punts;
UPDATE punts SET valor = 4 WHERE id = 50;
COMMIT;
SELECT valor FROM punts WHERE id = 50;
```
se inicia poniendo un valor a mad de (id,valor) = (50,5)
se inicia una transacion con begin, se intenta un select sin from,
se hace un update en punts para donde el id sea igual a 50 se poner un valor igual a 4, pero no se hace puers se esperaba por el final de transacion (???) se salva con un commit y se seleciona el valor donde el id es 50 que ahora sera igual a 5

```
* 6.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (60,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor) VALUES (61,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor) FROM punts;
```
se deleta todo de la tabla punts, se pone un {valor,id} de 60 y 5 respectivamente, se inicia una transacion, en seguida un update para tener un valor de 4 donde el id es 60, se hace un savepoint a, seguido de otro insert pero con (id,valor) de (61,8), se hace un savepoint b, otro insert con (id,valor) de (61,9), un rollback para b, commit y un select de la suma de los valor que retorna 12


**Erik Perdigones Garcia** <ins>iaw49185512</ins>  

**Cauan Goes Mateos** <ins>iaw29658501</ins>  

**Pol Núñez Rubio** <ins>iaw47671052</ins>  


--------------------------------------------
   Funcions de transaccions
--------------------------------------------

1.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  

```
INSERT INTO punts (id, valor) VALUES (10,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 10;
ROLLBACK;
SELECT valor FROM punts WHERE id = 10;
```  
**Se ha hecho un update donde cambiaba el valor, pero al hacer un rollback se vuelve al inicio de la transaccion, donde recupera su valor original.**

2.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  
```
INSERT INTO punts (id, valor) VALUES (20,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 20;
COMMIT;
SELECT valor FROM punts WHERE id = 20;
```
**Se guarda el nuevo valor introducido en la tabla, no se recupera el antiguo.**

3.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
INSERT INTO punts (id, valor) VALUES (30,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (31,7);
ROLLBACK;
SELECT valor FROM punts WHERE id = 30;
```  

**Mantiene el valor de 5 en la id 30, el rollback es desde el principio de la transaccion. Tampoco se crea la nueva fila de id x31.**

4.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (40,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*) FROM punts;
```
**Mantiene el valor nuevo de la id 40, pero el rollback es hasta el savepoint a, asi que la nueva fila con id 41 no aparecera.**  

5.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
INSERT INTO punts (id, valor) VALUES (50,5);
BEGIN;
SELECT id, valor WHERE punts;
UPDATE punts SET valor = 4 WHERE id = 50;
COMMIT;
SELECT valor FROM punts WHERE id = 50;
```
**El comando de la tercera linia da error, asi que hasta que no se finaliza la transicion no hay ningun tipo de update. Por eso se mantiene el resultado del primer valor introducido.**  

6.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (60,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor) VALUES (61,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor) FROM punts;
```  

**Se van realizando cambios hasta llegar al savepoint b, la siguiente orden no tendra influencia porque hay un rollback hasta el savepoint b, la suma mostrara la suma de 4 + 8, que respectivamente son las ids de 60 y 61.**  

7.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
DELETE FROM punts; -- Connexió 0
INSERT INTO punts (id, valor) VALUES (70,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

SELECT COUNT(*) FROM punts; -- Connexió 2
```  

**No podra ver la borrada de la tabla punts, hasta que no se finalice la transición. Asi que el usuario que haga select, le mostrara el resultado 1.**  

8.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
INSERT INTO punts (id, valor) VALUES (80,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (81,9); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 4 WHERE id = 80; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 81; -- Connexió 2

UPDATE punts SET valor = 10 WHERE id = 81; -- Connexió 1

UPDATE punts SET valor = 6 WHERE id = 80; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 80; -- Connexió 0
```  

**La conexion 2 no podra realizar su transaccion puesto que la connexión 1 empezo antes la transición y no ha hecho un commit, eso conlleva a que la gran mayoria de cambios el tendra la preferencia.**  

9.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
INSERT INTO punts (id, valor) VALUES (90,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

BEGIN; -- Connexió 2
INSERT INTO punts (id, valor) VALUES (90,9); -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 91; -- Connexió 0
```  

**Aunque en la conexxión uno se haga un borrado de la tabla, la connexión 2 esta creando una fila nueva que no exisita antes del borrado de la tabla. Por eso cuando inserta una nueva fila (id=91) no causa ningun tipo de error. En cambio la fila id=90 es borrada por el comando de la connexión 1.**

10.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
INSERT INTO punts (id, valor) VALUES (100,5); -- Connexió 0

grant 

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 100; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 100; -- Connexió 0
```
**EL usuario de la connexión 1 ha empezado la transacción antes, cambiando el valor del punto id=100. Como el usuario de la connexión 2 ha empezado la transacción más tarde y queria editar el valor que habia cambiado la connexión 1, se queda bloqueado hasta que el usuario de la connexión 1 finalice su transacción. El valor final sera 7.**

11.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
INSERT INTO punts (id, valor) VALUES (110,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (111,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 110; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 110; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 111; -- Connexió 2
SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 110; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 111; -- Connexió 0
```
**La connexión 1 comienza la transacción cambiando el valor de la id=110 y sin finalizar la transacción, la connexión 2 empieza una transacción cambiado también el valor de la id=110, lo que lleva a que se bloquee. Continua ejecutando comandos cambiando el valor de la id=111, haciendo un savepoint y volviendo a cambiar el valor del id=110. Pero luego la connexión 2 hace un rollback al savepoint a, cancelando el ultimo cambio de valor del id=110. Cuando la connexión 1 ejecuta el commit, se efectuan todos los cambios como en el ejercicio anterior y el resultado final muestra los valores que ha hecho la connexión 2 antes del savepoint. los valores finales son 7 y 7, respectivamente.**

12.- Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
```
INSERT INTO punts (id, valor) VALUES (120,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (121,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 121; -- Connexió 1
SAVEPOINT a;
UPDATE punts SET valor = 9 WHERE id = 120; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 120; -- Connexió 2

ROLLBACK TO a; -- Connexió 1

SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 120; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 121; -- Connexió 0
```
**La connexión 1 ha hecho un cambio de valor en el id=121 y a continuación hace un savepoint antes de cambiar el valor de id=120. Entonces la connexión 2 empieza una transacción haciendo un update del valor en la id=120 (cosa que llevara a que se bloquee). La novedad es que la connexión 1 hace rollback al savepoint, cancelando el update del valor de la id=120, que conlleva a que el update de la connexión 2 se ejecute, ya que el valor de la id=120 recupero su valor original antes de cualquier transacción. el valor de id=120 es 7 y de id=121 es 6.**







coalesce
current_date
current_timestamp

--1  BBDD Scott: Fes una consulta per treure del camp ename sense cap accentuació. S'han de contemplar totes les possibilitats : accents, dièresi, caràcters especials ç, ñ, majúscules i minúscules.
select translate(ename,'áéíóúçñàèòÁÉÍÓÚÇÑÀÈÒ','aeioucnaeoAEIOUCNAEO') from emp;

-- 2 BBDD TRAINING: Es demana una sentència SQL per a crear i carregar una taula que es dirà emp2 on, a més a més dels camps propis de la taula, tindrem el camp login ( que serà la inicial del nom i el cognom.)
-- a  Per a cada representant, mostrar el camp  nombre i la posició on comença el cognom
--select nombre, position( ' ' in nombre) + 1 "Posicion Apellido" from repventa;

-- b. Per cada representant, mostra el el camp nombre (tal com està a la base de dades)  i la columna Apellido
--select nombre, split_part(nombre, ' ', 2) "Apellido" from repventa;

-- c Per cada representant, mostra el camp nombre i el seu login
--select nombre, substring(nombre from 1 for 1) || split_part(nombre , ' ', 2) from repventa;
-- d  Ja podem fer l'exercici complet!

create table emp2 as select *, split_part(nombre, ' ', 2) "Login"  from repventa;


-- iaw29658501
-- Cauan Goes Mateos

drop function instr(origen varchar, patro varchar);
create or replace function instr(origen varchar, patro varchar)
returns varchar
AS $$
        BEGIN
                RETURN strpos(origen, patro);
        END;
$$ LANGUAGE plpgsql;

drop function strcat(cadena1 varchar, cadena2 varchar);
create or replace function strcat(cadena1 varchar, cadena2 varchar)
RETURNS varchar
AS $$
BEGIN
RETURN cadena1 || cadena2;
END;
$$ Language plpgsql;





--iaw29658501
--Cauan Goes Mateos
drop database if exists cauan;
create database cauan;
\c cauan;
drop function epoch();
create or replace function epoch() returns int
as $$

declare t interval;
declare seconds int ;
declare minutes int ;
declare hours int;
declare dias int;
begin
t = current_timestamp - '1970-01-01'::timestamp;
seconds = date_part('seconds', t)::int;
minutes = date_part('minutes',t)::int *60 ;
hours = date_part('seconds',t)::int * 3600 ;
dias = date_part('days',t)::int * 3600 * 24 ;
return seconds + minutes + hours + dias;
end;
$$ Language plpgsql;

select epoch();







--iaw29658501
--Cauan Goes Mateos
\c template1
drop database if exists cauan;
create database cauan;
\c cauan;
drop function epoch2( mydate timestamp );
create or replace function epoch2( mydate timestamp ) returns int
as $$
declare t interval;
declare seconds int ;
declare minutes int ;
declare hours int;
declare dias int;
begin
t = mydate - '1970-01-01'::timestamp;
seconds = date_part('seconds', t)::int;
minutes = date_part('minutes',t)::int *60 ;
hours = date_part('seconds',t)::int * 3600 ;
dias = date_part('days',t)::int * 3600 * 24 ;
return seconds + minutes + hours + dias;
end;
$$ Language plpgsql;

select epoch2(current_timestamp::timestamp);

drop function epoch3();
create or replace function epoch3() returns int as $$
declare t int;
begin
t = epoch2(current_timestamp::timestamp);
return t;
end;
$$ Language plpgsql;

select epoch3();









-- iaw29658501
-- Cauan Goes Mateos
-- 1. Creeu una funció que es digui darrerDiaMes, la qual mostrarà per pantalla el darrer dia del mes del mes actual. Sense arguments. Només podeu utilitzar funcions estandar (to_char)
\c template1
drop database cauan;
create database cauan;
\c cauan;

drop function darrerDiaMes();
CREATE OR REPLACE FUNCTION darrerDiaMes()
RETURNS VARCHAR
    AS $$
        declare year integer;
        declare month integer;
	BEGIN
            year := to_char(current_date,'YYYY');
            month := to_char(current_date,'MM');
        RETURN TO_CHAR(TO_DATE(year || '-' || month || '-01', 'YYYY-MM-DD') + INTERVAL '1 month' - INTERVAL '1 days','YYYY-MM-DD');
	END; $$
LANGUAGE plpgsql;

SELECT darrerDiaMes();
-- 2. Creeu la funció darrerDiaMes2 la qual, donat el més en numèric, mostrarà el darrer dia del mes.
DROP FUNCTION darrerDiaMes2( mon int );						
CREATE OR REPLACE FUNCTION darrerDiaMes2( mon int )
RETURNS VARCHAR
	AS $$ 
		declare year integer;
	BEGIN
		year := to_char(current_date,'YYYY');
	RETURN TO_CHAR(  TO_DATE(year || '-' || mon || '-01', 'yyyy-mm-dd') + INTERVAL '1 month' - INTERVAL '1 day', 'yyyy-mm-dd');
	END; $$
LANGUAGE plpgsql;
SELECT darrerDiaMes2(4);						
-- 3. Creeu la funció darrerDiaMes3, la qual donat un mes (text) passat com argument, mostrarà el darrer dia del mes.
DROP FUNCTION darrerDiaMes3( mon varchar );
CREATE OR REPLACE FUNCTION darrerDiaMes3( mon varchar )
RETURNS VARCHAR
	AS $$ 
		declare month integer;
	BEGIN
		month := mon::integer; 
	RETURN darrerDiaMes2(month);
	END; $$
LANGUAGE plpgsql;

SELECT darrerDiaMes3('4');
-- 4. Creeu la funció darrerDiaMesv2, que farà el mateix que darrerDiaMes i no tindrà arguments, utilitzant la funció darrerDiaMes2.     
DROP FUNCTION darrerDiaMesv2();
CREATE OR REPLACE FUNCTION darrerDiaMesv2()
RETURNS VARCHAR
    AS $$
        declare month integer;
	BEGIN
            month := to_char(current_date,'MM')::integer;
        RETURN darrerDiaMes2(month);
	END; $$
LANGUAGE plpgsql;
SELECT darrerDiaMesv2();










-- Cauan Goes
-- iaw29658501

drop function if exists dniCorrecte( p_dni varchar);
	
create or replace function dniCorrecte( p_dni varchar ) returns varchar
as $$
DECLARE v_numeros BIGINT;
BEGIN
	v_numeros := substring( p_dni from '[0-9]{8}' )::bigint;
	return v_numeros||substring('TRWAGMYFPDXBNJZSQVHLCKET',(MOD(v_numeros,23)+1)::int,1);
END;
$$ Language plpgsql;

drop function if exists dniCorrecte( p_dni bigint);

create or replace function dniCorrecte( p_dni bigint ) returns varchar
as $$
begin
	return dniCorrecte(p_dni::varchar);
end;
$$ Language plpgsql;

select dniCorrecte( '29658501' );
select dniCorrecte( 29658501 );












-- Cauan Goes Mateos
-- iaw29658501



\c training


create sequence if not exists cliecod_seq owned by cliente.cliecod;

select setval('cliecod_seq', (select max(cliecod) from cliente), true);
-- nextval('cliecod_seq')
create sequence if not exists pednum_seq owned by pedido.pednum;

select setval('pednum_seq', (select max(pednum) from pedido), true);


drop function existeixClient(p_id cliente.cliecod%type );
create or replace function existeixClient( p_id cliente.cliecod%type ) 
returns boolean as $$
	declare v_b smallint;
	begin
		select cliecod into strict v_b from cliente where cliecod = p_id;
		return true;
		exception
		when NO_DATA_FOUND then
			return false;
		END;
$$
LANGUAGE plpgsql;

select existeixClient(cast (2111 as smallint));


create or replace function altaClient(p_nombre varchar, p_repcod int, p_limcred cliente.limcred%type)
returns varchar
as $$
	begin
			insert into cliente(cliecod , nombre, repcod, limcred)
			values
			((select nextval('cliecod_seq')) , p_nombre, p_repcod, p_limcred);
			return 'Cleint '|| (select currval('cliecod_seq')) || ' afegit amb exit';
	end;
$$ language plpgsql;

--Comprovacion
select altaclient('Cau SL', (select repcod from cliente limit 1) , 37600);


create or replace function stockOk( p_cant int, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type ) returns boolean as $$
declare v_vassura producto.fabcod%type;
	begin
		select 'a' into strict v_vassura from producto where fabcod||prodcod=p_fabcod||p_prodcod and exist >= p_cant;
		return true;
		exception
			when NO_DATA_FOUND then
				return false;
	end;
$$
language plpgsql;

select stockOk( 1, (select fabcod from producto where exist>0 limit 1) , (select prodcod from producto where exist>0 limit 1));


create or replace function altaComanda ( p_cliecod pedido.cliecod%type , p_repcod pedido.repcod%type, p_fabcod pedido.fabcod%type, p_prodcod pedido.prodcod%type, p_cant pedido.cant%type) 
returns varchar as $$
declare message varchar;
declare v_import numeric(12,2);
begin
	if not existeixClient(p_cliecod) then
		raise exception using
			message:='Client ' || p_cliecod || ' not found';
	else
		if not stockOk(p_cant, p_fabcod, p_prodcod) then
			raise exception using
				message := p_prodcod || ' dont have ' || p_cant || ' quantity (nether more) ';

			else
				-- Let discover whats the total price (import)
			select precio * p_cant 
			into strict v_import 	
			from producto 
			where (fabcod, prodcod) = (p_fabcod, p_prodcod);
				-- Añadimos el pedido	
			insert into pedido 
			values ( ( select nextval('pednum_seq')) , current_date, p_cliecod , p_repcod , p_fabcod , p_prodcod, p_cant, v_import) ;
			update producto
			set exist = (exist - p_cant)
			where (fabcod, prodcod) = (p_fabcod, p_prodcod);

			update repventa
			set ventas = (ventas + v_import)
			where repcod = p_repcod;
			message := 'EL pedido num ' || (select currval('pednum_seq')) || ' creado con correctamente';
		end if;

	end if;
return message;
end;
$$
language plpgsql;


create or replace function updateVentas ( p_repcod repventa.repcod%type, p_ventas repventa.ventas%type)
returns varchar
as $$
begin
	update repventa 
	set ventas = ventas + p_ventas 
	where repcod = p_repcod;
	update oficina 
	set ventas = ventas + p_ventas 
	where ofinum = ( select ofinum 
			from repventa 
			where repcod = p_repcod);
	return 'Actualizado correctamente';
end;
$$
language plpgsql;
	select updateVentas(105::smallint, 10000);















-- iaw29658501 Cauan Goes
--\c biblioteca

create or replace function prestecDocument(p_idusuari prestec.idusuari%TYPE, p_iddocument exemplar.iddocument%TYPE ) returns boolean as $$
begin
-- Si el cliente ya tiene 4 o mas en este momento alugados o
-- si esta bloqueado no se podera prestar
	if ( ((select count(*) from prestec where idusuari=p_idusuari and datadev is null) >  3 )
		 or
		( select idusuari
		from usuari
		where bloquejat IS not null and
		idusuari=p_idusuari) > 0)
		THEN
			return false;
	end if;

-- si quiere alugar un libro y ya tiene dos alugados no podre
	if ((select 1 from llibre where iddocument=p_iddocument) > 0
		and
		(select count(*) from prestec where idusuari=p_idusuari and idexemplar in ( select idexemplar from llibre) ) >=  2 )
		THEN
			return false;
	end if;

-- si quiere alugar un cd y ya tiene un alugado no podra
	if ((select 1 from musica where iddocument=p_iddocument) > 0
		and
		(select count(*)
		from prestec
		where idusuari=p_idusuari and idexemplar in ( select idexemplar from musica))
		>=  1 )
		THEN
			return false;
	end if;


-- si quiere alugar un dvd y ya tiene un alugado no podra
	if ((select 1 from pelicula where iddocument=p_iddocument) > 0
		and
		(select count(*)
		from prestec
		where idusuari=p_idusuari and idexemplar in ( select idexemplar from pelicula))
		>=  1 )
		THEN
			return false;
	end if;

	return true;
end;
$$ language plpgsql;

insert into prestec values (1, current_timestamp, null, 2);

select prestecDocument(1,3);
select prestecDocument(2,5);




















-- iaw29658501 Cauan Goes
--\c biblioteca

create or replace function prestecDocument(p_idusuari prestec.idusuari%TYPE, p_iddocument exemplar.iddocument%TYPE ) returns boolean as $$
begin
-- Si el cliente ya tiene 4 o mas en este momento alugados o
-- si esta bloqueado no se podera prestar
	if ( ((select count(*) 
		from prestec 
		where idusuari=p_idusuari 
		and datadev is null) >  3 )
		 or
		( select idusuari
		from usuari
		where bloquejat IS not null and
		idusuari=p_idusuari) > 0)

		THEN
			RAISE NOTICE 'maximo de 4 por persona, tu ya tienes el maximo';
			return false;
	end if;

-- si quiere alugar un libro y ya tiene dos alugados no podre
	if ((select 1 
		from llibre 
		where iddocument=p_iddocument) > 0
		and
		(select count(*) 
		from prestec 
		where idusuari=p_idusuari 
		and idexemplar in 
			( select idexemplar 
			from llibre) ) >=  2 )

		THEN
			RAISE NOTICE 'MAXIMO DE DOS LIBROS POR PERSONA AL MISMO TIEMPO';
			return false;
	end if;

-- si quiere alugar un cd y ya tiene un alugado no podra
	if ((select 1 
		from musica 
		where iddocument=p_iddocument) > 0
		and
		(select count(*)
		from prestec
		where idusuari=p_idusuari 
		and idexemplar in 
			( select idexemplar from musica))
		>=  1 )
		THEN
			RAISE NOTICE 'MAXIMO DE UN CD POR PERSONA AL MISMO TIEMPO';
			return false;
	end if;


-- si quiere alugar un dvd y ya tiene un alugado no podra
	if ((select 1 
		from pelicula 
		where iddocument=p_iddocument) > 0
		and
		(select count(*)
		from prestec
		where idusuari=p_idusuari 
		and idexemplar in 
			( select idexemplar from pelicula))
		>=  1 )
		THEN
			RAISE NOTICE 'MAXIMO DE UN DVD POR PERSONA AL MISMO TIEMPO';
			return false;
	end if;
	return true;
end;
$$ language plpgsql;

insert into prestec values (1, current_timestamp, null, 2);

select prestecDocument(1,3);
select prestecDocument(2,5);


create or replace function renovarDocument(p_idexemplar exemplar.idexemplar%type) 
returns boolean as $$
declare v_idusuari prestec.idusuari%type;
begin
	select idusuari 
	INTO STRICT v_idusuari
	from prestec 
	where datadev is null 
	and idexemplar=p_idexemplar;	

IF ( select bloquejat from usuari where idusuari=v_idusuari ) then
		RAISE NOTICE 'Usuari % bloqueado', v_idusuari;
		return false;
end if;

	update prestec
        set datadev=current_timestamp
	where datadev is null
	and idexemplar=p_idexemplar;

	insert into prestec
	values ( p_idexemplar , current_timestamp, null, v_idusuari);	
	RAISE NOTICE 'Exemplar % renovado', p_idexemplar;
	return true;

end;
$$ language plpgsql;

insert into prestec values ( 2, current_timestamp, null, 5);
select renovarDocument(4);

create or replace function retornarDocument(p_iddocument document.iddocument%type, p_idusuari prestec.idusuari%type)
returns boolean as $$
declare v_idexemplar exemplar.idexemplar%type;
BEGIN
	select idexemplar 
	into strict v_idexemplar 
	from exemplar 
	where iddocument=p_iddocument;

	update prestec 
	set datadev=current_timestamp
	where idusuari=p_idusuari
	and idexemplar=v_idexemplar
	and datadev is null;
	raise notice 'Registrado retorno del documento % por %',p_iddocument, p_idusuari;

	return true;
END;
$$ language plpgsql;

insert into prestec values ( 6, current_timestamp, null, 5);
select retornarDocument(3,5);


create or replace function reservarDocument( p_iddocument document.iddocument%type, p_idusuari usuari.idusuari%type)
returns boolean as $$
declare v_idexemplar exemplar.idexemplar%type;
BEGIN
	select idexemplar
	into strict v_idexemplar 
	from exemplar
	where iddocument=p_iddocument 
	and datadev is null
	order by datapres 
	limit 1;
	insert into reserva values (v_idexemplar, p_idusuari, current_timestamp, null );
	raise notice 'Reserva del exemplar % adicionada para %',p_idexemplar, p_idusuari;
	return true;
end;
$$ language plpgsql;








--() { :; }; exec psql proves -f "$0"
-- iaw29658501 Cauan Goes Mateos

\c scott
CREATE OR REPLACE FUNCTION get_emp()
RETURNS text
AS $$
DECLARE
 titles TEXT DEFAULT '';
    -- declarem una variable fila anomenada rec_emp, la qual reculirà cada fila
    -- que es llegeix (fetch) del cursor
    -- Oracle permet %rowtype per taules i cursors, postgreSQL per taules només
 rec_emp   RECORD;
    -- Els cursors poden admetre paràmetres.
    -- Aquest és un exemple de cursor bàsic, sense paràmetres
 cur_emp CURSOR  IS               -- postgreSQL usa habitualment "FOR"
     SELECT ename, job     -- pero permet "IS" com a gest cap  a Oracle
     FROM emp;
BEGIN
	   -- Open the cursor
   OPEN cur_emp;
   LOOP
	      -- fetch row into the emp
      FETCH cur_emp INTO rec_emp;
      -- exit when no more row to fetch
      EXIT WHEN NOT FOUND;
      -- build the output
      IF true THEN
	         titles := titles || E'\n' || rpad(rec_emp.ename,30,' ')  || ':' || rec_emp.job;
		      END IF;
		   END LOOP;
		   -- Close the cursor
   CLOSE cur_emp;

   RETURN titles;
END;
$$ LANGUAGE plpgsql;


select get_emp();











-- solo podras bajar lo precio
drop trigger just_discount on product;
create trigger just_discount
before update on product
for each row
execute procedure just_discount();

