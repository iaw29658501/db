-- iaw29658501
-- Cauan Goes Mateos

create sequence if not exists empno_seq owned by emp.empno_seq;
select setval('empno_seq', ( select max(empno) from emp), true);

create sequence if not exists deptno_seq owned by dept.deptno increment by 10;
select setval('deptno_seq', (select max(deptno) from dept), true);

create or replace function fEmpleat() returns trigger as $$
declare v_deptno int;
begin
	-- El departamento existe  ???? 
	-- INTO STRICT peta si no retorna ningun valor
	SELECT deptno 
	INTO v_deptno
	FROM dept
	WHERE lower(dname) = lower(NEW.nomdept);
	if NOT FOUND 
	then
		v_deptno=nextval('deptno_seq');
		insert into dept 
		values (v_deptno, NEW.nomdept, NEW.localitat);
		raise notice E'EL departamento % no existia y ha sido creado', NEW.nomdept;
	end if;
	INSERT INTO emp 
	VALUES ( (select empno+1 from emp order by 1 desc limit 1) , NEW.nomemp, NEW.ofici, null, current_timestamp, NEW.salari, null, v_deptno);
	raise notice E'Empleado nom % creado', NEW.nomemp;
	return NEW;
end;
$$ language plpgsql;


drop trigger tEmpleat on empleat;

create trigger tEmpleat
instead of insert on empleat
for each row
execute procedure fInsertEmp();

insert into empleat values ('Erik', 1500, 'DEVELOPER', 'DEVELOPMENT', 'MARTE');
insert into empleat values ('Pol', 1200, 'DEVELOPER', 'RESEARCH', 'CHICAGO');

select * from emp;

