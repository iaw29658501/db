
# el update recibe como primero argumento la query (donde) , despues los nuevas definiciones clave:valor, seguida de opciones adicionales 
# como el multi
# ejemplo, para cambiar el cognom para Turbando donde el nombre comienze con T

db.students.update({name:/^T/},  {$set:{'lastname1':'Turbando'}})



#  Para todo documento que el nombre comienze con name va a cambiar el "lastname2" para "Sant" ( multi para tener efecto en todas las ocurencias)

db.students.update({name:/^Ad/},  {$set:{'lastname2':'Sant'}},{multi:true})

