-- Cauan Góes Mateos iaw29658501
drop table Museo,SalaMuseo,obra,autor cascade;
create table Museo(
	codmuseo varchar(32) constraint codmuseo_pk primary key,
	nombre varchar(64),
	direccion varchar(128),
	ciudad varchar(64),
	pais varchar(64)
);

create table SalaMuseo(
	codmuseo varchar(128),
	idsala varchar(64)
);

create table obra(
	codobra varchar(32) constraint codobra_pk primary key,
	titulo varchar(64),
	tipo varchar(32),
	codmuseo varchar(128),
	idsala varchar(64)
);

create table autor(
	codautor varchar(32),
	nombre varchar(128),
	nacionalidad varchar(64),
	constraint codautor_pk primary key autor(codautor)
);
alter table SalaMuseo add contraint
alter table obra add constraint obra_codmuseo_idsala_fk foreign key (codmuseo,idsala) references SalaMuseo(
