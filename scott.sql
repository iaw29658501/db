-- Cauan
-- iaw29658501
-- 19/12/2018
--scott.sql
-- 26 Calcular el salario total mensual.

select sum(sal)  "salario total" 
from emp;

-- 27 Calcular el número de empleados que tienen comisión y la media. Queremos mostrar todos los empleados, con lo que tened en cuenta que el campo comm puede tener nulos.

select count(*) "Empleados con comission", round(avg(coalesce(comm,0)),2) "Media de comission"
from emp 
where comm is not null;

--28 Seleccionar el salario, mínimo y máximo de los empleados, agrupados por empleo.
select job "trabajo", min(sal) "salario minimo", max(sal) "salario maximo" 
from emp 
group by job;

--29 Siguiendo lo explicado en el ej. 27, Seleccionar por cada departamento, el numero de empleados que tienen comision, la suma y la media.
select deptno, count(*) "Cuantos tienen comision",
sum(comm) "suma comision", round(avg(comm),2) "media comision" 
from emp 
where comm is not null 
group by deptno ;

--30  Idem que el 4, pero mostrando además el nombre de departamento.
select ename, e.deptno, dname 
from emp e, dept d 
where e.deptno != 30 
and d.deptno=e.deptno;

--31 Seleccionar el salario mínimo, máximo y medio de los empleados agrupados por empleo, pero sólo de aquellos cuya media sea superior a 4000.

select min(sal) "salario min" , max(sal) "sal max", avg(sal) "media salarial" 
from emp 
group by job 
having avg(sal) > 4000;

--32 Visualice el número y el nombre de los departamentos que tengan más de tres empleados asignados.

select deptno, dname from  dept where deptno in (select deptno from emp where deptno is not null group by deptno having count(*) > 3);

-- 33 Seleccionar los empleados que trabajan en el mismo departamento que Clark.

select * from emp where deptno=(select deptno from emp where ename = 'CLARK');

-- 34 Calcular el número de empleados por departamento que tienen un salario superior a la media.

select * from emp join (select deptno, avg(sal) media from emp e where deptno is not null group by deptno) as e on emp.deptno=e.deptno where sal > media;

-- 35 Seleccionar los empleados cuyo salario sea superior al de Adams.
select * from emp where sal > (select sal from emp where ename = 'ADAMS');

-- 36    Seleccionar el nombre y fecha de ingreso del empleado que lleva menos tiempo.

select ename, hiredate from emp where hiredate = (select min(hiredate) from emp);

-- 37 Seleccionar el nombre de los empleados que ganen más que todos los que trabajan de salesman.

select ename from emp where sal > (select max(sal) from emp where job='SALESMAN');

-- 38  Seleccionar los empleados que ganen más que alguno de los que trabajan de salesman.

select ename 
from emp 
where sal > (select min(sal) 
from emp 
where job='SALESMAN');



-- 39  Mostrar el trabajo, el nombre y el salario de los empleados ordenados por el tipo de trabajo y por salario descendente.

select job, ename, sal 
from emp 
order by job,sal desc;

-- 40   Seleccionar el nombre de cada empleado, y el número y nombre de su jefe.

select emp.ename, emp.mgr, jefe.ename 
from emp, emp jefe 
where emp.mgr=jefe.empno;

-- 41 Mostrar el nombre del empleado y su fecha de alta en la empresa de los empleados que son analyst.

select ename, hiredate 
from emp 
where job='ANALYST';

-- 42  Mostrar el nombre del empleado y una columna que contenga el salario multiplicado por la comisión cuya cabecera sea ‘BONO’.

select ename, sal + coalesce(comm,0) "bono" 
from emp;

-- 43 Encontrar el salario medio de aquellos empleados cuyo trabajo sea el de analista (analyst).

select round(avg(sal),2) 
from emp 
where job='ANALYST';

-- 44 Encontrar el salario más alto, el más bajo y la diferencia entre ambos.

select max(sal), 
	min(sal), 
	max(sal) - min(sal) "delta" 
from emp;


-- 45 Hallar el numero de trabajos distintos que existen en el departamento 30.

select count(distinct job) 
from emp 
where deptno=30;

-- 46 Mostrar el nombre del empleado, su trabajo, el nombre y el código del departamento en el que trabaja.

select ename, job, emp.deptno, d.dname from emp, dept d where d.deptno=emp.deptno;

-- 47 Mostrar el nombre, el trabajo y el salario de todos los empleados que tienen un salario superior al salario más bajo del departamento 30.

select ename, job, sal from emp where sal > (select min(sal) from emp where deptno=30);

-- 48 Encontrar a los empleados cuyo jefe es ‘BLAKE’.

select * from emp where mgr = (select empno from emp where ename='BLAKE');

-- 49 Encontrar el nº de trabajadores diferentes en el departamento 30 para aquellos empleados cuyo salario pertenezca al intervalo [1000, 1800].

select count(*) from emp where sal between 1000 and 1800;

-- 50 Encontrar el ename, dname, job y sal de los empleados que trabajen en el mismo departamento que ‘TURNER’ y su salario sea mayor que la media del salario del departamento 10.

select ename, d.dname, e.job, e.sal from emp e, dept d where e.deptno=(select deptno from emp where ename='TURNER') and e.sal > (select avg(sal) from emp where deptno=e.deptno);

/*
51     ndíquese si las siguientes sentencias son correctas, si no lo son, indique en qué consiste el error:

SELECT * FROM EMP

WHERE MGR = NULL;

-- NO ES CORRECTA PUES NULL NO ES COMPARABLE 

SELECT * FROM DEPT

WHERE DEPTNO = 20 OR WHERE DEPTNO = 30;

-- NO ES CORRECTO POR EL WHERE DOS VECES EN LA MISMA CONSULTA

SELECT * FROM EMP

WHERE NOT ENAME LIKE ‘R%’

AND SAL BETWEEN 3000 AND 5000;

-- SQL SE ESCRIVE IGUAL SE HABLARIA ...
-- WHERE NOT ENAME LIKE ESTA MAL ES CIRTO ES
-- WHETE ENAME NOT LIKE 'R%'

SELECT * FROM EMP

WHERE SAL < 4000 AND JOB NOT = ’ANALYST’;
-- no esta ciero el cierto seria
-- where sal < 4000 and not job = 'ANALYST'; 

SELECT * FROM DEPT

WHERE LOC = ‘DALLAS’ OR ‘CHICAGO’;
-- where loc = 'DALLAR OR loc = 'CHICAGO';
*/



-- 52  El salario medio y mínimo de cada puesto, mostrando en el resultado aquellos cuyo  salario medio esté por encima de 1500.

select job, round(avg(sal),2) "Salario medio" from emp group by job having avg(sal) > 1500 ;

-- 53  ¿Qué empleados trabajan en ‘DALLAS’ ?

select e.* from emp e, dept d where e.deptno=d.deptno and d.loc='DALLAS';

-- 54 ¿Cuántos empleado trabajan en ‘CHICAGO’ ?
-- R:6
select count(*) from emp e, dept d where e.deptno=d.deptno and d.loc='CHICAGO';

-- 55    Listar el nombre de los empleados que ganan menos que sus supervisores.
-- Y se fuera para correr todas las informaciones pero solo de una tabla ?
select e.ename from emp e, emp s where e.mgr=s.empno and e.sal < s.sal;

-- 56 Listar el nombre, trabajo, departamento, localidad y salario de aquellos empleados que tengan un salario mayor de 2000 y trabajen en ‘DALLAS’ o ‘NEW YORK’.

select e.ename, e.job, e.deptno, d.loc from emp e, dept d where d.deptno=e.deptno and sal > 2000 and d.loc='NEW YORK';



-- 57 Insertar en la tabla DEPT la información correspondiente a un nuevo departamento de consultoría, cuyo número sea 50 y que esté ubicado en SANTANDER.

insert into dept values (50, null,'SANT ANDER');

-- 58 Dar de alta a un nuevo empleado de nombre BELTRAN, que desempeñará el puesto de analyst en el departamento de SALES y cuyo número de empleado sea 8200. Por el momento se desconoce su salario y quién será su jefe.

insert into emp values ( 8200, 'BELTRAN', 'ANALYST', null, current_date, null, null, (select deptno from dept where loc='SALES'));


-- 59 Cambiar la fecha del empleado SCOTT por la de hoy.

update emp set hiredate=current_date where ename='SCOTT' ;
    
-- 60 El empleado MILLER, debido a sus éxitos es ascendido al puesto de analyst,aumentándole su salario en un 20%, cambiándole al departamento ‘SALES’ e integrándole en el grupo que supervisa el empleado 7566. Haced los cambios oportunos .

update emp set sal=(select sal from emp where ename='MILLER') * 1.2, job='ANALYST', deptno=(select deptno from dept where dname='SALES') where ename='MILLER';

-- 61 A raíz de la firma del convenio anual de la empresa, se ha determinado incrementar el salario de todos los empleados en un 6%. Incorporar esta cambio a la base de datos.


-- 62 El empleado JAMES causa baja en la empresa. Borrad la información correspondiente

delete from emp where enmae='JAMES';

-- 63 Se contrata a SANZ, con número 1657, para el departamento 30 y con sueldo 3000.

insert into emp (ename, empno, deptno, sal ) values ('SANZ', 1657, 30, 3000);

-- 64 SANZ cambia al departamento 40.

update emp set deptno=40 where ename='SANZ';
-- 65  SANZ trabaja de vendedor, con una comisión de 4000.

update emp set job='SALESMAN' where ename='SANZ';
-- 66 Se decide aumentar las comisiones. Aumentan todas las comisiones  en un 20% del salario.

update emp set comm=coalesce(comm,0) + 0.2*coalesce(sal,0);

-- 67 Se decide aumentar un 35% el salario a los empleados que ganen menos que SANZ.

update emp set sal=sal*1.35 where sal < (select sal from emp where ename='SANZ');

-- 68  Se despide a SANZ.
delete from emp where ename='SANZ';
-- 69  Se despide a los que trabajan en el departamento ‘SALES’

delete from emp where deptno = (select dept.deptno from dept where dname='SALES');

